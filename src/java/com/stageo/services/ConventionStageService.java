/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.services;

import com.stageo.controleur.GenererConventionStageAction;
import com.stageo.dao.AdresseDAO;
import com.stageo.dao.CompagnieDAO;
import com.stageo.dao.EmployeurDAO;
import com.stageo.dao.EtudiantDAO;
import com.stageo.dao.StageDAO;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Adresse;
import com.stageo.model.Candidature;
import com.stageo.model.Compagnie;
import com.stageo.model.Employeur;
import com.stageo.model.Etudiant;
import com.stageo.model.OffreStage;
import com.stageo.model.Professeur;
import com.util.Util;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 *
 * @author guill
 */
public class ConventionStageService {
    private String entrepriseNom, entrepriseAdresse, employeurNom, employeurPrenom, 
            coordonnateurNom, coordonnateurPrenom, stagiaireNom, stagiairePrenom,
            stagiaireProfil, dateDebutStage, dateFinStage, stageNombreSemaines,
            stageNombreJoursSemaine, stageNombreHeuresSemaine, stageHoraireTravail,
            stageFonction, stageObjectifs;
    
    public ConventionStageService(){
        entrepriseNom = "'_ENTREPRISENOM'";
        entrepriseAdresse = "'_ENTREPRISEADRESSE'";
        employeurNom = "'_EMPLOYEURNOM'";
        employeurPrenom = "'_EMPLOYEURPRENOM'";
        coordonnateurNom = "'_COORDONNATEURNOM'";
        coordonnateurPrenom = "'_COORDONNATEURPRENOM'";
        stagiaireNom = "'_STAGIAIRENOM'";
        stagiairePrenom = "'_STAGIAIREPRENOM'";
        stagiaireProfil = "'_STAGIAIREPROFIL'";
        dateDebutStage = "'_DATEDEBUTSTAGE'";
        dateFinStage = "'_DATEFINSTAGE'";
        stageNombreSemaines = "'_STAGENOMBRESEMAINES'";
        stageNombreJoursSemaine = "'_STAGENOMBREJOURSSEMAINE'";
        stageNombreHeuresSemaine = "'_STAGENOMBREHEURESSEMAINE'";
        stageHoraireTravail = "'_STAGEHORAIRETRAVAIL'";
        stageFonction = "'_STAGEFONCTION'";
        stageObjectifs = "'_STAGEOBJECTIFS'";
    }
    
    public ConventionStageService(Candidature candidature, Professeur coordonnateur) throws SQLException{
        Connection cnx = Connexion.getInstance();
        EmployeurDAO empDAO = new EmployeurDAO(cnx);
        CompagnieDAO comDAO = new CompagnieDAO(cnx);
        AdresseDAO adrDAO = new AdresseDAO(cnx);
        EtudiantDAO etuDAO = new EtudiantDAO(cnx);
        StageDAO stageDAO = new StageDAO(cnx);
        
        OffreStage stage = stageDAO.read(candidature.getIdOffre());
        Employeur employeur = empDAO.read(stage.getIdEmployeur());
        Compagnie compagnie = comDAO.read(employeur.getCompagnie().getIdCompagnie());
        Adresse adresse = adrDAO.read(compagnie.getAdresse().getIdAdresse());
        Etudiant stagiaire = etuDAO.read(candidature.getIdEtudiant());
        
        entrepriseNom = compagnie.getNom();
        entrepriseAdresse = adresse.toString();
        employeurNom = employeur.getNom();
        employeurPrenom = employeur.getPrenom();
        coordonnateurNom = coordonnateur.getNom();
        coordonnateurPrenom = coordonnateur.getPrenom();
        stagiaireNom = stagiaire.getNom();
        stagiairePrenom = stagiaire.getPrenom();
        stagiaireProfil = stagiaire.getProfilEtudes();
        dateDebutStage = stage.getDateDebut().toString();
        dateFinStage = stage.getDateFin().toString();
        stageNombreSemaines = "'_STAGENOMBRESEMAINES'";
        stageNombreJoursSemaine = "'_STAGENOMBREJOURSSEMAINE'";
        stageNombreHeuresSemaine = "'_STAGENOMBREHEURESSEMAINE'";
        stageHoraireTravail = "'_STAGEHORAIRETRAVAIL'";
        stageFonction = stage.getTitre();
        stageObjectifs = stage.getDescription();
    }

    public void setEntrepriseNom(String entrepriseNom) {
        this.entrepriseNom = entrepriseNom;
    }

    public void setEntrepriseAdresse(String entrepriseAdresse) {
        this.entrepriseAdresse = entrepriseAdresse;
    }

    public void setEmployeurNom(String employeurNom) {
        this.employeurNom = employeurNom;
    }

    public void setEmployeurPrenom(String employeurPrenom) {
        this.employeurPrenom = employeurPrenom;
    }

    public void setCoordonnateurNom(String coordonnateurNom) {
        this.coordonnateurNom = coordonnateurNom;
    }

    public void setCoordonnateurPrenom(String coordonnateurPrenom) {
        this.coordonnateurPrenom = coordonnateurPrenom;
    }

    public void setStagiaireNom(String stagiaireNom) {
        this.stagiaireNom = stagiaireNom;
    }

    public void setStagiairePrenom(String stagiairePrenom) {
        this.stagiairePrenom = stagiairePrenom;
    }

    public void setStagiaireProfil(String stagiaireProfil) {
        this.stagiaireProfil = stagiaireProfil;
    }

    public void setDateDebutStage(String dateDebutStage) {
        this.dateDebutStage = dateDebutStage;
    }

    public void setDateFinStage(String dateFinStage) {
        this.dateFinStage = dateFinStage;
    }

    public void setStageNombreSemaines(String stageNombreSemaines) {
        this.stageNombreSemaines = stageNombreSemaines;
    }

    public void setStageNombreJoursSemaine(String stageNombreJoursSemaine) {
        this.stageNombreJoursSemaine = stageNombreJoursSemaine;
    }

    public void setStageNombreHeuresSemaine(String stageNombreHeuresSemaine) {
        this.stageNombreHeuresSemaine = stageNombreHeuresSemaine;
    }

    public void setStageHoraireTravail(String stageHoraireTravail) {
        this.stageHoraireTravail = stageHoraireTravail;
    }

    public void setStageFonction(String stageFonction) {
        this.stageFonction = stageFonction;
    }

    public void setStageObjectifs(String stageObjectifs) {
        this.stageObjectifs = stageObjectifs;
    }
    
    public boolean generer() throws IOException{
        JFileChooser fileSaver = new JFileChooser();
        
        fileSaver.addChoosableFileFilter(new FileNameExtensionFilter("Microsoft Word Documents (*.docx)", "docx"));
        fileSaver.setAcceptAllFileFilterUsed(true);
                
        int choix = fileSaver.showSaveDialog(null);
        
        if (choix == JFileChooser.APPROVE_OPTION){
            try {
                XWPFDocument document = new XWPFDocument();
                
                File fichier = fileSaver.getSelectedFile();
                System.out.println(fichier.getName());
                if (!(fichier.getName().contains(".docx"))){
                    fichier = new File(fileSaver.getSelectedFile() + ".docx");
                }
                FileOutputStream out = new FileOutputStream(fichier);
                
                // Titre
                XWPFParagraph paragraph1 = document.createParagraph();
                paragraph1.setAlignment(ParagraphAlignment.CENTER);
                XWPFRun paragraph1Run = paragraph1.createRun();
                paragraph1Run.setBold(true);
                paragraph1Run.setText("CONVENTION DE STAGE");
                paragraph1Run.addBreak();
                paragraph1Run.setText("DEC Techniques informatique");
                paragraph1Run.addBreak();
                paragraph1Run.addBreak();
                
                // Identification
                XWPFParagraph paragraph2 = document.createParagraph();
                XWPFRun paragraph2Run = paragraph2.createRun();
                paragraph2Run.setText("INTERVENUE ENTRE : ");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText(entrepriseNom);
                paragraph2Run.addBreak();
                paragraph2Run.setText(entrepriseAdresse);
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("Dûment représenté(e) par " + employeurNom + " " + employeurPrenom);
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("CI-APRÈS DÉSIGNÉ(E) L'ENTREPRISE");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("ET");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("LA CORPORATION DU COLLÈGE D'ENSEIGNEMENT GÉNÉRAL ET PROFESSIONNEL DE ROSEMONT,"
                        + "corps légalement constitué en corporation, en vertu de la loi sur les collèges d'enseignement général"
                        + " et professionnel (L.R.Q.,chapitre C-29) ayant son siège social au numéro 6400 de la seizième (16e) "
                        + "Avenue dans la ville de Montréal, province de Québec, H1X 2S9;");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("Dûment représenté(e) par " + coordonnateurPrenom + " " + coordonnateurNom);
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("CI-APRÈS DÉSIGNÉ LE COLLÈGE");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("IDENTIFICATION DE L'ÉTUDIANT STAGIAIRE");
                paragraph2Run.addBreak();
                paragraph2Run.addBreak();
                paragraph2Run.setText("Nom : " + stagiaireNom);
                paragraph2Run.addBreak();
                paragraph2Run.setText("Prénom : " + stagiairePrenom);
                paragraph2Run.addBreak();
                paragraph2Run.setText("Programme : " + Util.toUTF8(stagiaireProfil));
                
                // Identification stage
                XWPFParagraph paragraph3 = document.createParagraph();
                XWPFRun paragraph3Run = paragraph3.createRun();
                paragraph3Run.setText("IL A ÉTÉ CONVENU CE QUI SUIT : ");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("La période de stage est prévue du " + dateDebutStage + " au " + dateFinStage);
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("MODALITÉS");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("1. L'ENTREPRISE accepte de recevoir l'étudiant(e) stagiaire pour une péridode de "
                        + stageNombreSemaines + " semaines à raison de " + stageNombreJoursSemaine + " jours par semaine"
                        + " à raison de " + stageNombreHeuresSemaine + " heures/semaine. L'horaire de travail sera : "
                        + stageHoraireTravail + ".");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("2. Le responsable de stage accepte au nom de L'ENTREPRISE, que l'étudiant(e)-stagiaire exerce sous supervision la ou les fonctions de : ");
                paragraph3Run.addBreak();
                paragraph3Run.setText(stageFonction);
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("Le projet de stage comprend les objectifs suivants : ");
                paragraph3Run.addBreak();
                paragraph3Run.setText(stageObjectifs);
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("L'ENTREPRISE reconnaît avoir pris connaissance des objectifs de stage et s'engage à fournir un cadre de travail favorable à sa démarche.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("L'étudiant(e)-stagiaire reconnaît être responsable de ses apprentissages.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("3. Les responsables de l'encadrement des stages seront : ");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText(employeurPrenom + " " + employeurNom + " pour L'ENTREPRISE");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("ET");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText(coordonnateurPrenom + " " + coordonnateurNom + " pour LE COLLÈGE");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("4. L'étudiant(e)-stagiaire est entièrement protégé(e) par notre assurance responsabilité civile,"
                        + " No 0860000-certificat no: 028, pour toute la durée du stage, et par la loi des accidents du travail du Québec (CNESST).");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("5. Durant la période de stage, l'étudiant demeure et doit être considéré(e) comme élève du COLLÈGE. "
                        + "Par contre, il doit respecter l'horaire de travail de L'ENTREPRISE.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("6. Durant son stage, l'étudiant doit respecter les politiques et les règlements de L'ENTREPRISE ainsi que le secret professionnel afférent.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("7. En cas d'indiscipline, d'absence de motivation ou autre difficulté de comportement du stagiaire, "
                        + "L'ENTREPRISE doit informer le COLLÈGE qui verra à donner un avertissement au stagiaire. En cas de récidive, le stage sera interrompu,"
                        + " et ce, sans préjudice pour l'une ou l'autre des parties.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("8. L'ENTREPRISE s'engage à fournir au superviseur de stage du COLLÈGE l'évaluation du stagiaire.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("9. Le COLLÈGE s'engage à remettre à L'ENTREPRISE une copie du contrat signé par les deux parties "
                        + "ainsi que toute documentation pertinente relative au stage.");
                paragraph3Run.addBreak();
                paragraph3Run.addBreak();
                paragraph3Run.setText("10. La signature de la convention de stage implique pour les signataires un consentement exprès aux clauses de cette convention.");
                
                // Signatures
                XWPFParagraph paragraph4 = document.createParagraph();
                XWPFRun paragraph4Run = paragraph4.createRun();
                paragraph4Run.setText("FAIT À MONTRÉAL le " + Date.valueOf(LocalDate.now()));
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("(Avec mention 'lu et approuvé')");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("L'ENTREPRISE");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("Représenté par ");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText(employeurPrenom + " " + employeurNom);
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("Responsable de stage");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText(employeurPrenom + " " + employeurNom);
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("COLLÈGE DE ROSEMONT");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("Représenté par ");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText(coordonnateurPrenom + " " + coordonnateurNom);
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText("L'étudiant(e)-stagiaire");
                paragraph4Run.addBreak();
                paragraph4Run.addBreak();
                paragraph4Run.setText(stagiairePrenom + " " + stagiaireNom);
                
                document.write(out);
                out.close();
                return true;
                
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GenererConventionStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        return false;
    }
}
