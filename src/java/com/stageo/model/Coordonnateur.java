/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author guill
 */
public class Coordonnateur extends Professeur{
    private List<Document> liste_document;
    private List<Notification> liste_notification;
    
    public Coordonnateur(){
        super();
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
    }
    
    public Coordonnateur( String idUtilisateur,
                        String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(idUtilisateur, email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
    }
    
    public Coordonnateur( String email,
                        String nomUtilisateur,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        super(email, nomUtilisateur, motDePasse, nom, prenom, role);
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
    }
    
    public List<Document> getListe_document() {
        return liste_document;
    }

    public void setListe_document(List<Document> liste_document) {
        this.liste_document = liste_document;
    }

    public List<Notification> getListe_notification() {
        return liste_notification;
    }

    public void setListe_notification(List<Notification> liste_notification) {
        this.liste_notification = liste_notification;
    }
}
