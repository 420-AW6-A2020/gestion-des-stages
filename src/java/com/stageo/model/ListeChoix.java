/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Guillaume
 */

public class ListeChoix {
    private static final List<String> listeOuiNon = new ArrayList<>();
    static {
        listeOuiNon.add("Oui");
        listeOuiNon.add("Non");
    }
    
    private static final List<String> listeMoyenTransport = new ArrayList<>();
    static {
        listeMoyenTransport.add("Auto");
        listeMoyenTransport.add("Transport en commun");
        listeMoyenTransport.add("Transport en commun, Auto");
    }
    
    private static final List<String> listeChoixAnglais = new ArrayList<>();
    static {
        listeChoixAnglais.add("Excellente");
        listeChoixAnglais.add("Correcte");
        listeChoixAnglais.add("Faible");
        listeChoixAnglais.add("Nulle");
    }
    
    private static final List<String> listeDemarches = new ArrayList<>();
    static {
        listeDemarches.add("Oui");
        listeDemarches.add("Non");
        listeDemarches.add("Stage trouvé");
    }
    
    private static final List<String> listeInfosPartagees = new ArrayList<>();
    static {
        listeInfosPartagees.add("Téléphone seulement");
        listeInfosPartagees.add("Courriel seulement");
        listeInfosPartagees.add("Courriel et téléphone");
    }
    
    private static final List<String> listePreferenceSupervision = new ArrayList<>();
    static {
        listePreferenceSupervision.add("Réseau");
        listePreferenceSupervision.add("Programmation");
        listePreferenceSupervision.add("Réseau en priorité puis Programmation");
        listePreferenceSupervision.add("Programmation en priorité puis Réseau");
        listePreferenceSupervision.add("Réseau ou Programmation");
    }
    
    private static final List<String> listeTailleEntreprise = new ArrayList<>();
    static {
        listeTailleEntreprise.add("Petite");
        listeTailleEntreprise.add("Moyenne");
        listeTailleEntreprise.add("Grande");
        listeTailleEntreprise.add("Très grande");
    }
    
    private static final List<String> listeProfilEtudes = new ArrayList<>();
    static {
        listeProfilEtudes.add("420-S1R-RO / 420.21 : Gestion de réseau - Parcours de continuité");
        listeProfilEtudes.add("420-S1R-RO / 420.AC : Gestion de réseau");
        listeProfilEtudes.add("420-S4J-RO / 420.22 : Informatique de gestion - Parcours de continuité");
        listeProfilEtudes.add("420-S4J-RO / 420.AA : Informatique de gestion");
    }

    public static List<String> getListeOuiNon() {
        return listeOuiNon;
    }

    public static List<String> getListeMoyenTransport() {
        return listeMoyenTransport;
    }

    public static List<String> getListeChoixAnglais() {
        return listeChoixAnglais;
    }

    public static List<String> getListeDemarches() {
        return listeDemarches;
    }

    public static List<String> getListeInfosPartagees() {
        return listeInfosPartagees;
    }

    public static List<String> getListePreferenceSupervision() {
        return listePreferenceSupervision;
    }

    public static List<String> getListeTailleEntreprise() {
        return listeTailleEntreprise;
    }

    public static List<String> getListeProfilEtudes() {
        return listeProfilEtudes;
    }
    
}
