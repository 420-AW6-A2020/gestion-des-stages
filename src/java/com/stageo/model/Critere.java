/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;

/**
 *
 * @author admin
 */
public class Critere {
    private String IdCritere,
                   Nom;
    
    public Critere() {}

    public Critere(String IdCritere, String Nom) {
        this.IdCritere = IdCritere;
        this.Nom = Nom;
    }

    public Critere(String Nom) {
        String uniqueID = UUID.randomUUID().toString();
        this.IdCritere = uniqueID;
        this.Nom = Nom;
    }

    public String getIdCritere() {
        return IdCritere;
    }

    public void setIdCritere(String IdCritere) {
        this.IdCritere = IdCritere;
    }

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }
    
    
    
}
