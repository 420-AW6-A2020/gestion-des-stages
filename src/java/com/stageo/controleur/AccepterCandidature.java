/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

/**
 *
 * @author AC/JP
 */
import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.model.Etudiant;
import com.stageo.dao.EtudiantDAO;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import com.util.Util;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AccepterCandidature implements Action , RequestAware, SessionAware, RequirePRGAction, DataSender{
    
    private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        
        String action = "*.do?tache=afficherPageListeStagiaires";
            try{
                String  email = request.getParameter("email"),
                        prenom = request.getParameter("prenom"),
                        nom = request.getParameter("nom");
                    
                Connection cnx = Connexion.getInstance();
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                System.out.println("Test");
                System.out.println(email);
                System.out.println(prenom);
                System.out.println(nom);
                Utilisateur user = dao.findByCourriel(email);
                // A corriger plus tard : lorsque le coordonnateur devra accepter une candidature
//                user.setStatutRecherche("Candidature acceptee");
                dao.update(user);
                
            }
            catch(SQLException ex){
                Logger.getLogger(EffectuerInscriptionAction.class.getName()).log(Level.SEVERE, null, ex);
            }
            return action;
        }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}

    

