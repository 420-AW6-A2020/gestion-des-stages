/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.dao.StageDAO;
import com.stageo.dao.CritereDAO;
import com.stageo.model.*;
import com.util.Util;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.sql.rowset.serial.SerialBlob;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author AC
 */
public class AjouterOffreDeStage implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {

    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        
        String action = "*.do?tache=afficherPageCreationStage";
        boolean erreur = false;
       
        try {
            String titre = request.getParameter("titre"),
                    desc = request.getParameter("desc"),
                    lienWeb = request.getParameter("lienWeb"),
                    document = request.getParameter("document");

            int remuneration = Integer.parseInt(request.getParameter("remuneration"));

            // Conversion d'un objet Date et Timestamp afin de l'envoyer a la bd
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd");

            Date date_debut = formatter.parse(request.getParameter("date_debut"));
            Date date_fin = formatter.parse(request.getParameter("date_fin"));

            Timestamp timeStampDateDebut = new Timestamp(date_debut.getTime());
            Timestamp timeStampDateFin = new Timestamp(date_fin.getTime());
            
            Connection cnx = Connexion.getInstance();
            Part filePart;
            filePart = request.getPart("fichier");
            
            StageDAO dao = new StageDAO(cnx);
            OffreStage offreStage = new OffreStage();
            
            InputStream filecontent = filePart.getInputStream();
            List<Critere> listeCritereStage = new ArrayList<>();
            CritereDAO cDAO= new CritereDAO(cnx);
          
            List<Critere> ListeTousCritere= cDAO.findAll();
            
            for (int i = 0; i < ListeTousCritere.size(); i++){
                    
                    String checked = request.getParameter(ListeTousCritere.get(i).getIdCritere());
                    //verifie que la valeur est cochée
                    if (checked!=null){
                    listeCritereStage.add(ListeTousCritere.get(i));
                    }
                }
            
                
            System.out.println(listeCritereStage);
            offreStage.setTitre(titre);
            offreStage.setDescription(desc);
            offreStage.setLienWeb(lienWeb);
            offreStage.setLienDocument(document);
            offreStage.setRemunere(remuneration);
            offreStage.setDateDebut(timeStampDateDebut);
            offreStage.setDateFin(timeStampDateFin);
            offreStage.setFichier(filecontent);
            offreStage.setIdEmployeur((String) session.getAttribute("connecte"));
            offreStage.setListeCritere(listeCritereStage);
            dao.create(offreStage);


        } catch (SQLException ex) {
            Logger.getLogger(EffectuerInscriptionAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AjouterOffreDeStage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AjouterOffreDeStage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(AjouterOffreDeStage.class.getName()).log(Level.SEVERE, null, ex);
        }

        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
