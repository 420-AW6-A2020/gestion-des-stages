/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.stageo.model.Document;
import com.stageo.dao.DocumentDAO;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author Chris
*/
public class SupprimerDocumentAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {
    
    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;
    
    @Override
    public String execute() {

        try {
            DocumentDAO dao = new DocumentDAO(Connexion.getInstance());
            Document doc = new Document();
            doc.setIdDocument((String)request.getParameter("id"));
            dao.delete(doc);
        } catch (SQLException ex) {
            Logger.getLogger(SupprimerDocumentAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "?tache=afficherPageDocument";
    }
    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
