package com.stageo.controleur;

import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import com.stageo.model.OffreStage;
import com.stageo.dao.StageDAO;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Config;
import com.stageo.jdbc.Connexion;

/**
 *
 * @author JP
 */
public class EffectuerAcceptationStageAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender{
    

   private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        
        String action = "*.do?tache=afficherPageListeStage";

            System.out.println("Test1");
            System.out.println(request.getParameter("idOffre"));
            if((request.getParameter("idOffre") != null)){
            System.out.println("Test2");
             
            try{
                
                Connection cnx = Connexion.getInstance();
                session = request.getSession(true);
                 
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = dao.read((String) session.getAttribute("connecte"));
                
                StageDAO stageDao = new StageDAO(cnx);
                OffreStage stage = new OffreStage();
                
                String  idOffre = request.getParameter("idOffre"),
                        idUser = user.getIdUtilisateur();
               
                stage.setIdOffre(idOffre);
                stage.setStatutOffre(true);
                stageDao.updateStatutOffre(stage);
                
                data.put("message", "Le stage <strong>" + stage.getTitre()+ "</strong> a été accepté.");
                data.put("messageType", "Succes");
                
                data.put("tacheEffectuee",true);
                      
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerAcceptationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}