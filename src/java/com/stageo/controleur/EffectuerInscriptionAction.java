/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

/**
 *
 * @author Dave
 */
import com.stageo.DataSender;
import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.SessionAware;
import com.stageo.Action;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Connexion;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EffectuerInscriptionAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {

    //private HttpSession session;
    private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        System.out.println("JE SUIS DANS EffectuerInscriptionAction.java !!!");

        boolean erreur = false;
        boolean creer_user = true;
        String action = ".do?tache=afficherPageInscription";

        if (!erreur) {
            try {
                String username = request.getParameter("username"),
                        motPasse = request.getParameter("motDePasse"),
                        role = request.getParameter("role");

                Connection cnx = Connexion.getInstance();
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = new Utilisateur();
                user.setNomUtilisateur(username);
                user.setMotDePasse(motPasse);
                user.setRole(role);

                if (username != null) {
                    //faire vérification avec des findBy
                    if (dao.findByUsername(username) != null) {
                        data.put("message", "Ce nom d'utilisateur est déjà utilisé.");
                        data.put("messageType", "Echec");
                    }
                    else {
                        creer_user = dao.create(user);
                        if (creer_user) {
                            user = dao.findByIdentifiantMotPasse(username, motPasse);
                            // Afficher un message a l'utilisateur confirmant la creation du compte
                            data.put("message", "Le compte <strong>" + user.getRole() + " (" + user.getNomUtilisateur() + ")</strong> a bien été créé.");
                            data.put("messageType", "Succes");
                        } else {
                            data.put("message", "Problème de création du compte. Veuillez réessayer. Si le problème survient à répétition, contactez un administrateur.");
                            data.put("messageType", "Echec");
                        }
                    }
                    data.put("tacheEffectuee", true);
                }

            } catch (SQLException ex) {
                Logger.getLogger(EffectuerInscriptionAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
