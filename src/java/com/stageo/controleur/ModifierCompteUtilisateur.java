/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.RequirePRGAction;
import com.stageo.RequestAware;
import com.stageo.Action;
import com.stageo.DataSender;
import com.stageo.model.Utilisateur;
import com.stageo.dao.UtilisateurDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.stageo.jdbc.Connexion;
import com.util.PasswordHash;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

/**
 *
 * @author Nicole
 */

public class ModifierCompteUtilisateur implements Action, RequestAware, RequirePRGAction, DataSender {
    private HttpServletRequest request;
    private HttpSession session;
    
    private HashMap data;
    

    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageProfil";
        
        String hash = "";
        String act = request.getParameter("Enregistrer");

        String ancienPassword = request.getParameter("AncienPassword"); 
        String nouveauPassword = request.getParameter("NouveauPassword"); 
        String confirmerPassword = request.getParameter("ConfirmationPassword");
        
        String ancienUsername = request.getParameter("AncienUsername");
        String nouveauUsername = request.getParameter("NouveauUsername");
        
        session = request.getSession(true);
        
        String utilisateurID = (String)session.getAttribute("connecte"),
               email = request.getParameter("email"),
               prenom = request.getParameter("prenom"),
               nom = request.getParameter("nom"),
               nomUtil =request.getParameter("username"),
               mot_de_passe =request.getParameter("mot_de_passe");
        
        try {
            Connection connect = Connexion.getInstance();
            UtilisateurDAO utilisateur = new UtilisateurDAO(connect);
            Utilisateur util = utilisateur.read(utilisateurID);
            
            if("EnregistrerUsername".equals(act)){
                
                if(util.getNomUtilisateur().equals(ancienUsername)){
                    if(utilisateur.findByUsername(nouveauUsername) != null && utilisateur.findByUsername(nouveauUsername).getNomUtilisateur().equals(nouveauUsername)){
                        data.put("message", "Ce nom d'utilisateur est déjà utilisé.");
                        data.put("messageType", "Echec");
                    }
                    else {
                        util.setNomUtilisateur(nouveauUsername);
                        utilisateur.update_nom_utilisateur(util);
                        
                        data.put("message", "Votre nom d'utilisateur a été modifié.");
                        data.put("messageType", "Succes");
                    }
                    
                }
                else{
                    data.put("message", "L'ancien nom d'utilisateur entré est invalide.");
                    data.put("messageType", "Echec");
                }
            
            }
            else if("EnregistrerPassword".equals(act)){                
                if(util.getMotDePasse().equals(ancienPassword)){

                    if(nouveauPassword.equals(confirmerPassword)){
                        util.setMotDePasse(confirmerPassword);      
                        utilisateur.update_mot_de_passe(util);
                        
                        data.put("message", "Votre mot de passe a été modifié.");
                        data.put("messageType", "Succes");
                    }
                    else{
                        data.put("message", "Les deux mots de passe ne sont pas identiques.");
                        data.put("messageType", "Echec");
                    }
                }
                else{
                    data.put("message", "L'ancien mot de passe entré est invalide.");
                    data.put("messageType", "Echec");
                }
            
            }
//            hash = PasswordHash.createHash(confirmerPassword);
//            util.setMotDePasse(hash);
            data.put("tacheEffectuee", true);
        
        } catch (SQLException ex) {
            Logger.getLogger(ModifierCompteUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return action;
    }

   

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
   
    
}
