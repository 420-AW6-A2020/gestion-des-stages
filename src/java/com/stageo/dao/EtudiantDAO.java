/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.Candidature;
import com.stageo.model.Critere;
import com.stageo.model.Etudiant;
import com.stageo.model.Utilisateur;
import com.util.Util;

/**
 *
 * @author Dave
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EtudiantDAO extends DAO<Etudiant> {

    public EtudiantDAO() {
    }

    public EtudiantDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Etudiant x) {
        String req = "INSERT INTO etudiant (`ID_ETUDIANT`, `STATUT_RECHERCHE`, `PROFIL_CREE`) "
                + "VALUES (?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())) {
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));

                if (x.getStatutRecherche() != null && !"".equals(x.getStatutRecherche().trim())) {
                    paramStm.setString(2, Util.toUTF8(x.getStatutRecherche()));
                } else {
                    paramStm.setString(2, "en cours");
                }

                paramStm.setDate(3, Date.valueOf(LocalDate.now()));

                int nbLignesAffectees = paramStm.executeUpdate();

                if (nbLignesAffectees > 0) {
                    paramStm.close();
                    return true;
                }
            }
            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EtudiantDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public Etudiant read(String id) {
        String req = "SELECT etudiant.*, utilisateur.* FROM etudiant, utilisateur "
                + "WHERE `ID_ETUDIANT` = ? AND `ID_UTILISATEUR` = `ID_ETUDIANT`";

        String req2 = "SELECT etudiantcritere.*, critere.* FROM etudiantcritere, critere "
                + "WHERE `ID_ETUDIANT` = ? AND critere.ID_CRITERE = etudiantcritere.ID_CRITERE";

        String req3 = "SELECT * FROM candidature WHERE `ID_ETUDIANT` = ?";

        PreparedStatement paramStm = null;
        try {

            // Execution de la 1ere requete
            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Etudiant etudiant = new Etudiant();

                etudiant.setNom((resultat.getString("NOM")));
                etudiant.setPrenom((resultat.getString("PRENOM")));
                etudiant.setEmail((resultat.getString("COURRIEL")));
                etudiant.setNomUtilisateur((resultat.getString("USERNAME")));
                etudiant.setTelephone((resultat.getString("TELEPHONE")));
                etudiant.setRole((resultat.getString("TYPE_UTILISATEUR")));

                etudiant.setIdUtilisateur((resultat.getString("ID_ETUDIANT")));
                etudiant.setVille((resultat.getString("VILLE")));
                etudiant.setProfilEtudes((resultat.getString("PROFIL_ETUDES")));
                etudiant.setStageSession((resultat.getString("STAGE_SESSION")));
                etudiant.setAnglaisEcrit((resultat.getString("ANGLAIS_ECRIT")));
                etudiant.setAnglaisParle((resultat.getString("ANGLAIS_PARLE")));
                etudiant.setDemarcheEnCours((resultat.getString("DEMARCHE_EN_COURS")));
                etudiant.setPermisConduire((resultat.getString("PERMIS_CONDUIRE")));
                etudiant.setLogin365((resultat.getString("LOGIN365")));
                etudiant.setNumeroDA((resultat.getString("NUMERO_DA")));
                etudiant.setMoyenTransport((resultat.getString("MOYEN_TRANSPORT")));
                etudiant.setPreferences((resultat.getString("PREFERENCES")));
                etudiant.setProfilCree((resultat.getString("PROFIL_CREE")));
                etudiant.setProfilModifie((resultat.getString("PROFIL_MODIFIE")));

                if ((etudiant.getStatutRecherche() != null) && (!"".equals(etudiant.getStatutRecherche().trim()))) {
                    etudiant.setStatutRecherche((resultat.getString("STATUT_RECHERCHE")));
                } else {
                    etudiant.setStatutRecherche("en cours");
                }

                // 2e requete : SELECT etudiantcritere.*, critere.* FROM etudiantcritere, critere "
                // "WHERE `ID_ETUDIANT` = ? AND critere.ID_CRITERE = etudiantcritere.ID_CRITERE
                paramStm = cnx.prepareStatement(req2);
                paramStm.setString(1, id);
                resultat = paramStm.executeQuery();

                List<Critere> listeCriteres = new ArrayList<>();
                Critere critere;
                while (resultat.next()) {
                    critere = new Critere((resultat.getString("ID_CRITERE")), (resultat.getString("NOM")));
                    listeCriteres.add(critere);
                }
                etudiant.setListeCritere(listeCriteres);

                // Execution de la 3e requete
                paramStm = cnx.prepareStatement(req3);
                paramStm.setString(1, id);
                resultat = paramStm.executeQuery();

                List<Candidature> listeCandidature = new ArrayList<Candidature>();
                Candidature c;
                while (resultat.next()) {
                    c = new Candidature((resultat.getString("ID_OFFRE")), resultat.getTimestamp("DATE"), (resultat.getString("STATUT")));
                    listeCandidature.add(c);

                }
                etudiant.setListe_candidature(listeCandidature);

                resultat.close();
                paramStm.close();
                return etudiant;

            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
            System.out.println("Erreur 1 SQL (classe etudiantDAO) :" + exp);
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
                System.out.println("Erreur 2 SQL (classe etudiantDAO) :" + exp);
            } catch (Exception e) {
                System.out.println("Erreur 3 (classe etudiantDAO) :" + e);
            }
        }

        return null;

    }

    @Override
    public boolean update(Etudiant x) {

        String req1 = "UPDATE etudiant SET `VILLE` = ?, `PROFIL_ETUDES` = ?, `STAGE_SESSION` = ?, "
                + "`ANGLAIS_ECRIT` = ?, `ANGLAIS_PARLE` = ?, `DEMARCHE_EN_COURS` = ?, `PERMIS_CONDUIRE` = ?,"
                + "`LOGIN365` = ?, `NUMERO_DA` = ?, `MOYEN_TRANSPORT` = ?, `PREFERENCES` = ?,"
                + " `STATUT_RECHERCHE` = ?, `PROFIL_MODIFIE` = ? WHERE `ID_ETUDIANT` = ?";

        String req1a = "UPDATE utilisateur SET `NOM` = ?, `PRENOM` = ?, `TELEPHONE` = ?, `COURRIEL` = ? WHERE `ID_UTILISATEUR` = ?";

        String req2 = "DELETE FROM etudiantCritere WHERE `ID_ETUDIANT` = ?";

        String req3 = "INSERT INTO etudiantCritere (`ID_ETUDIANT`, `ID_CRITERE`) VALUES (?,?)";

        PreparedStatement paramStm;
        try {
            // Execution de la 1ere requete
            paramStm = cnx.prepareStatement(req1);
            paramStm.setString(1, x.getVille());
            paramStm.setString(2, x.getProfilEtudes());
            paramStm.setString(3, x.getStageSession());
            paramStm.setString(4, x.getAnglaisEcrit());
            paramStm.setString(5, x.getAnglaisParle());
            paramStm.setString(6, x.getDemarcheEnCours());
            paramStm.setString(7, x.getPermisConduire());
            paramStm.setString(8, x.getLogin365());
            paramStm.setString(9, x.getNumeroDA());
            paramStm.setString(10, x.getMoyenTransport());
            paramStm.setString(11, x.getPreferences());
            paramStm.setString(12, x.getStatutRecherche());
            paramStm.setDate(13, Date.valueOf(LocalDate.now()));
            paramStm.setString(14, x.getIdUtilisateur());
            int nbLignesAffecteesUpdate = paramStm.executeUpdate();

            // Execution de la 1ere requete a
            paramStm = cnx.prepareStatement(req1a);
            paramStm.setString(1, x.getNom());
            paramStm.setString(2, x.getPrenom());
            paramStm.setString(3, x.getTelephone());
            paramStm.setString(4, x.getEmail());
            paramStm.setString(5, x.getIdUtilisateur());
            int nbLignesAffecteesUpdate1a = paramStm.executeUpdate();

            // Execution de la 2e requete
            paramStm = cnx.prepareStatement(req2);
            paramStm.setString(1, x.getIdUtilisateur());
            int nbLignesAffecteesDelete = paramStm.executeUpdate();

            // Execution de la 3e requete
            int nbLignesCriteresAffecteesUpdate = 0;
            for (int i = 0; i < x.getListeCritere().size(); i++) {
                paramStm = cnx.prepareStatement(req3);
                paramStm.setString(1, x.getIdUtilisateur());
                paramStm.setString(2, x.getListeCritere().get(i).getIdCritere());
                nbLignesCriteresAffecteesUpdate = paramStm.executeUpdate();
            }
            paramStm.close();

            if (nbLignesAffecteesUpdate > 0 || nbLignesCriteresAffecteesUpdate > 0 || nbLignesAffecteesUpdate1a > 0) {
                return true;
            }
        } catch (SQLException exp) {
            Logger.getLogger(EtudiantDAO.class.getName())
                    .log(Level.SEVERE, null, exp);
        }
        return false;
    }

    @Override
    public boolean delete(Etudiant x) {
        String req = "DELETE FROM etudiant WHERE `ID_ETUDIANT` = ?";

        PreparedStatement paramStm = null;

        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdUtilisateur());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } catch (Exception exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EtudiantDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public List<Etudiant> findAll() {

        List<Etudiant> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM etudiant");
            while (r.next()) {
                Etudiant etudiant = new Etudiant();
                etudiant.setIdUtilisateur(r.getString("ID_ETUDIANT"));
                
                etudiant.setVille((r.getString("VILLE")));
                etudiant.setProfilEtudes((r.getString("PROFIL_ETUDES")));
                etudiant.setStageSession((r.getString("STAGE_SESSION")));
                etudiant.setAnglaisEcrit((r.getString("ANGLAIS_ECRIT")));
                etudiant.setAnglaisParle((r.getString("ANGLAIS_PARLE")));
                etudiant.setDemarcheEnCours((r.getString("DEMARCHE_EN_COURS")));
                etudiant.setPermisConduire((r.getString("PERMIS_CONDUIRE")));
                etudiant.setLogin365((r.getString("LOGIN365")));
                etudiant.setNumeroDA((r.getString("NUMERO_DA")));
                etudiant.setMoyenTransport((r.getString("MOYEN_TRANSPORT")));
                etudiant.setPreferences((r.getString("PREFERENCES")));
                etudiant.setProfilCree((r.getString("PROFIL_CREE")));
                etudiant.setProfilModifie((r.getString("PROFIL_MODIFIE")));
                etudiant.setStatutRecherche(r.getString("STATUT_RECHERCHE"));

                liste.add(etudiant);
            }
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;

    }

    public Utilisateur findByStatutRecherche(String statutRecherche) {
        String req = "SELECT * FROM etudiant WHERE `STATUT_RECHERCHE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(statutRecherche));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Etudiant etudiant = new Etudiant();
                etudiant.setIdUtilisateur(resultat.getString("ID_ETUDIANT"));
                etudiant.setStatutRecherche(resultat.getString("STATUT_RECHERCHEw"));

                resultat.close();
                paramStm.close();
                return etudiant;
            }

            resultat.close();
            paramStm.close();
            return null;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            }
        }
        return null;
    }

    public List<Utilisateur> findStageTrouve(String statutRecherche) {

        List<Utilisateur> liste = new LinkedList<>();
        PreparedStatement paramStm = null;
        String req = "SELECT * FROM etudiant WHERE `STATUT_RECHERCHE` = ?";
        try {
            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(statutRecherche));

            ResultSet resultat = paramStm.executeQuery();
            while (resultat.next()) {
                Utilisateur user = new Utilisateur();
                user.setIdUtilisateur((resultat.getString("ID_UTILISATEUR")));
                user.setEmail((resultat.getString("COURRIEL")));
                user.setNomUtilisateur((resultat.getString("USERNAME")));
                user.setMotDePasse((resultat.getString("MOT_DE_PASSE")));
                user.setNom((resultat.getString("NOM")));
                user.setPrenom((resultat.getString("PRENOM")));
                user.setRole((resultat.getString("TYPE_UTILISATEUR")));
                user.setEtatCompte(resultat.getBoolean("ETAT_COMPTE"));
                liste.add(user);
            }
            resultat.close();
            paramStm.close();
        } catch (SQLException ex) {
            Logger.getLogger(EtudiantDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        return liste;

    }

    @Override
    public Etudiant read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
