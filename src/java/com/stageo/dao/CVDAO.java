/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.dao;

import com.stageo.model.CV;
import com.util.Util;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JP
 */
public class CVDAO extends DAO<CV>{
       
    public CVDAO() {
    }
    
    public CVDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(CV x) {
        String req = "INSERT INTO cv (`ID_CV`,`LANGUE`,`NB_VUES`,`ID_ETUDIANT`,`DATE`,`FICHIER`) VALUES (?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(x.getIdCv()));
            paramStm.setString(2, Util.toUTF8(x.getLangue()));
            paramStm.setInt(3, x.getNbVues());
            paramStm.setString(4, Util.toUTF8(x.getIdEtudiant()));
            paramStm.setString(5, Util.toUTF8(x.getDate())); 
            paramStm.setBinaryStream(7, x.getFichier());
            

            int nbLignesAffectees= paramStm.executeUpdate();

            if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
            }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CVDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }

    @Override
    public CV read(int id) {
        String req = "SELECT * FROM cv WHERE `ID_CV` = ?";
        
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if(resultat.next()){

                CV c = new CV();
                c.setIdCv(resultat.getString("ID_CV"));
                c.setLangue(resultat.getString("LANGUE"));
                c.setNbVues(resultat.getInt("NB_VUES"));
                c.setIdEtudiant(resultat.getString("ID_ETUDIANT"));
                c.setDate(resultat.getString("DATE"));
                c.setFichier(resultat.getBinaryStream("FICHIER"));
      
                
                resultat.close();
                paramStm.close();
                    return c;
            }
            
            resultat.close();
            paramStm.close();
            return null;
                
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }        
        
        return null;
    }

    @Override
    public CV read(String id) {
        
        try{
            return this.read(Integer.parseInt(id));
        }
        catch(NumberFormatException e){
            return null;
        }

    }

    @Override
    public boolean update(CV x) {
        String req = "UPDATE cv SET `LANGUE` = ?, `NB_VUES` = ?,`ID_ETUDIANT` = ?,`DATE` = ?,`FICHIER` = ? WHERE `ID_CV` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getIdCv() == null || "".equals(x.getIdCv().trim()))
                    paramStm.setString(1, null);
                else
                    paramStm.setString(1, Util.toUTF8(x.getIdCv()));
                    paramStm.setString(2, Util.toUTF8(x.getLangue()));
                    paramStm.setInt(3, x.getNbVues());
                    paramStm.setString(4, Util.toUTF8(x.getIdEtudiant()));
                    paramStm.setString(5, Util.toUTF8(x.getDate())); 
                    paramStm.setBinaryStream(7, x.getFichier());

                int nbLignesAffectees= paramStm.executeUpdate();
                
                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CVDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }

    @Override
    public boolean delete(CV x) {
        String req = "DELETE FROM cv WHERE `ID_CV` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdCv());
                
                int nbLignesAffectees= paramStm.executeUpdate();
                
                if (nbLignesAffectees>0) {
                        paramStm.close();
                    return true;
                }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CVDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }

   
    @Override 
    public List<CV> findAll() {
        List<CV> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement(); 
            ResultSet r = stm.executeQuery("SELECT * FROM cv");
            while (r.next()) {
                CV c = new CV();
                c.setIdCv(r.getString("ID_CV"));
                c.setLangue(r.getString("LANGUE"));
                c.setNbVues(r.getInt("NB_VUES"));
                c.setIdEtudiant(r.getString("ID_ETUDIANT"));
                c.setDate(r.getString("DATE"));
                c.setFichier(r.getBinaryStream("FICHIER"));
                           			
                liste.add(c);
            }
           // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException exp) {
        }
        return liste;
    } 

    public CV findByIdEtudiant(String idEtudiant) {
        String req = "SELECT * FROM cv WHERE `ID_ETUDIANT` = ?";
        
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);      
            paramStm.setString(1, Util.toUTF8(idEtudiant));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if(resultat.next()){

                CV c = new CV();
                c.setIdCv(resultat.getString("ID_CV"));
                c.setLangue(resultat.getString("LANGUE"));
                c.setNbVues(resultat.getInt("NB_VUES"));
                c.setIdEtudiant(idEtudiant);
                c.setDate(resultat.getString("DATE"));
                c.setFichier(resultat.getBinaryStream("FICHIER"));

                resultat.close();
                paramStm.close();
                    return c;
            }
            
            resultat.close();
            paramStm.close();
            return null;
                
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }        
        
        return null;
    }
    
}