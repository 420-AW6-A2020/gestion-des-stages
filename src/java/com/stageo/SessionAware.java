
package com.stageo;

import javax.servlet.http.HttpSession;
/**
 *
 * @author Dave
 */
public interface SessionAware {
    public void setSession(HttpSession session);
}
