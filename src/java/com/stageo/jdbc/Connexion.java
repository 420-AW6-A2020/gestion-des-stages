package com.stageo.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Dave
 */
public class Connexion {
	private static Connection connection;
	private static String url;
	private static String user;		
        private static String password;
        private static String driver;

        public Connexion(){
        }
        
	public static Connection getInstance() throws SQLException {
            try{
                if (connection == null || connection.isClosed()){
                    if (user.equals(""))
                        connection = DriverManager.getConnection(url);
                    else
                        connection = DriverManager.getConnection(url,user,password);
                }
            }
            catch(SQLException e){
                System.out.println("erreur connexion");
                System.out.println(e);
                connection = null;
            }
            return connection;
	}
        
        public Connection getConnection() throws SQLException, ClassNotFoundException{
            return Connexion.startConnection(user, password, url, driver);
        }
        
	public static void reinit(){
            close();
            connection = null;
	}
        
	public static void close()
	{
            try {
                if (connection!=null) {
                    connection.close();
                    connection = null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
	}
	public static String getUrl() {
		return url;
	}
	public static void setUrl(String url) {
		Connexion.url = url;
	}     
        
	public static String getUser() {
		return user;
	}
	public static void setUser(String user) {
		Connexion.user = user;
	}
	public static void setPassword(String password) {
		Connexion.password = password;
	}

        public static String getDriver() {
            return driver;
        }

        public static void setDriver(String driver) {
            Connexion.driver = driver;
        }

        
        public static void setConfig(String user, String password, String url){
                Connexion.user = user;
                Connexion.password = password;
                Connexion.url = url;
        }
        
        public static Connection startConnection(String user, String password, String url, String driver) throws ClassNotFoundException, SQLException{
                Class.forName(driver);
                Connexion.user = user;
                Connexion.password = password;
                Connexion.url = url;             
            return getInstance();
        }    
}

