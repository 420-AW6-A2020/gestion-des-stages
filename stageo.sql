-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 17 Décembre 2019 à 02:55
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `stageo`
--

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

CREATE TABLE `adresse` (
  `ID_ADRESSE` varchar(64) NOT NULL,
  `ADRESSE_LIGNE1` varchar(128) DEFAULT NULL,
  `ADRESSE_LIGNE2` varchar(128) DEFAULT NULL,
  `VILLE` varchar(64) DEFAULT NULL,
  `CODE_POSTAL` varchar(16) DEFAULT NULL,
  `PROVINCE` varchar(32) DEFAULT NULL,
  `PAYS` varchar(32) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adresse`
--

INSERT INTO `adresse` (`ID_ADRESSE`, `ADRESSE_LIGNE1`, `ADRESSE_LIGNE2`, `VILLE`, `CODE_POSTAL`, `PROVINCE`, `PAYS`) VALUES
('f7899d13-6841-4942-a447-e0f47a9f6efe', '1517 rue des Samares', '', 'Longueuil', 'J4M 0A9', 'QC', 'Canada'),
('993917ed-50cc-44cb-b589-545ff0b5f6a7', '1234, fausse rue', '', 'MontrÃ©al', 'G4N 2G4', 'QC', 'Canada');

-- --------------------------------------------------------

--
-- Structure de la table `candidature`
--

CREATE TABLE `candidature` (
  `ID_ETUDIANT` varchar(64) NOT NULL,
  `ID_OFFRE` varchar(64) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUT` varchar(32) NOT NULL,
  `ID_PROFESSEUR` varchar(64) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `compagnie`
--

CREATE TABLE `compagnie` (
  `ID_COMPAGNIE` varchar(64) NOT NULL,
  `NOM` varchar(128) DEFAULT NULL,
  `SITE_WEB` varchar(256) DEFAULT NULL,
  `SECTEUR` varchar(64) DEFAULT NULL,
  `TEL_ENTREPRISE` varchar(32) DEFAULT NULL,
  `LIEN_GOOGLE_MAPS` varchar(512) DEFAULT NULL,
  `KILOMETRAGE` int(6) DEFAULT NULL,
  `TAILLE` varchar(32) DEFAULT NULL,
  `NB_EMPLOYES` int(12) DEFAULT NULL,
  `ID_ADRESSE` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `compagnie`
--

INSERT INTO `compagnie` (`ID_COMPAGNIE`, `NOM`, `SITE_WEB`, `SECTEUR`, `TEL_ENTREPRISE`, `LIEN_GOOGLE_MAPS`, `KILOMETRAGE`, `TAILLE`, `NB_EMPLOYES`, `ID_ADRESSE`) VALUES
('b21d03be-4231-4332-b3fc-5ab1d1663c52', 'Desjardins', 'http://www.Desjardins.ca', 'Finances', '514-000-1234', 'https://desjardins.ca', 30, 'Moyenne', 250, '993917ed-50cc-44cb-b589-545ff0b5f6a7'),
('db70546f-51bd-4dc2-8540-5c811ed7dda7', 'Ubisoft', 'https://www.Ubisoft.ca', 'Informatique', '514-865-8745', NULL, 20, 'Petite', 6, 'f7899d13-6841-4942-a447-e0f47a9f6efe');

-- --------------------------------------------------------

--
-- Structure de la table `coordonnateur`
--

CREATE TABLE `coordonnateur` (
  `ID_COORDONNATEUR` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `critere`
--

CREATE TABLE `critere` (
  `ID_CRITERE` varchar(64) NOT NULL,
  `NOM` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `critere`
--

INSERT INTO `critere` (`ID_CRITERE`, `NOM`) VALUES
('1', 'PhP'),
('2', 'Java Web'),
('3', 'Angular '),
('4', 'CSS'),
('5', 'Mysql '),
('6', 'Nodejs');

-- --------------------------------------------------------

--
-- Structure de la table `cv`
--

CREATE TABLE `cv` (
  `ID_CV` varchar(64) NOT NULL,
  `LIEN` varchar(256) NOT NULL,
  `LANGUE` varchar(32) NOT NULL,
  `NB_VUES` int(11) NOT NULL,
  `ID_ETUDIANT` varchar(64) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FICHIER` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE `document` (
  `ID_DOCUMENT` varchar(64) NOT NULL,
  `TITRE` varchar(255) NOT NULL,
  `LIEN` varchar(32) NOT NULL,
  `TYPE` varchar(128) NOT NULL,
  `NB_VUES` int(11) NOT NULL,
  `ID_COORDONNATEUR` varchar(64) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FICHIER` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employeur`
--

CREATE TABLE `employeur` (
  `ID_EMPLOYEUR` varchar(64) NOT NULL,
  `ID_COMPAGNIE` varchar(64) NOT NULL,
  `FONCTION` varchar(128) DEFAULT NULL,
  `PROFIL_CREE` date DEFAULT NULL,
  `PROFIL_MODIFIE` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `employeur`
--

INSERT INTO `employeur` (`ID_EMPLOYEUR`, `ID_COMPAGNIE`, `FONCTION`, `PROFIL_CREE`, `PROFIL_MODIFIE`) VALUES
('1dd808de-19f3-4485-86c2-a3c2f766fae6', 'b21d03be-4231-4332-b3fc-5ab1d1663c52', 'RH', NULL, '2019-12-16'),
('ddcc6988-4bcc-4cb1-b756-2fa709727698', 'db70546f-51bd-4dc2-8540-5c811ed7dda7', 'Ressources humaines', NULL, '2019-11-13');

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `ID_ETUDIANT` varchar(64) NOT NULL,
  `VILLE` varchar(64) DEFAULT NULL,
  `PROFIL_ETUDES` varchar(256) DEFAULT NULL,
  `STAGE_SESSION` varchar(64) DEFAULT NULL,
  `ANGLAIS_ECRIT` varchar(64) DEFAULT NULL,
  `ANGLAIS_PARLE` varchar(64) DEFAULT NULL,
  `DEMARCHE_EN_COURS` varchar(64) DEFAULT NULL,
  `PERMIS_CONDUIRE` varchar(64) DEFAULT NULL,
  `LOGIN365` varchar(64) DEFAULT NULL,
  `NUMERO_DA` varchar(32) DEFAULT NULL,
  `MOYEN_TRANSPORT` varchar(32) DEFAULT NULL,
  `PREFERENCES` varchar(1024) DEFAULT NULL,
  `STATUT_RECHERCHE` varchar(32) NOT NULL,
  `PROFIL_CREE` date DEFAULT NULL,
  `PROFIL_MODIFIE` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etudiant`
--

INSERT INTO `etudiant` (`ID_ETUDIANT`, `VILLE`, `PROFIL_ETUDES`, `STAGE_SESSION`, `ANGLAIS_ECRIT`, `ANGLAIS_PARLE`, `DEMARCHE_EN_COURS`, `PERMIS_CONDUIRE`, `LOGIN365`, `NUMERO_DA`, `MOYEN_TRANSPORT`, `PREFERENCES`, `STATUT_RECHERCHE`, `PROFIL_CREE`, `PROFIL_MODIFIE`) VALUES
('10de2895-0cbb-4437-8dd7-1e39e9ec8052', 'Quebec', '420-S4J-RO / 420.22 : Informatique de gestion - Parcours de continuitÃ©', 'H-2020', 'Correcte', 'Correcte', 'Non', 'Non', '1231212@crosemont.qc.ca', '1231212', 'Transport en commun', 'DÃ©veloppement web', 'en cours', NULL, NULL),
('7be38f86-916a-4525-98ba-97be67fd5097', 'Longueuil', '420-S4J-RO / 420.22 : Informatique de gestion - Parcours de continuitÃ©', 'H-2020', 'Correcte', 'Correcte', 'Non', 'Oui', '1688203@crosemont.qc.ca', '1688203', 'Transport en commun, Auto', 'DÃ©veloppement web', 'en cours', '2019-10-21', '2019-11-13'),
('c68e7315-e44b-4a30-8a49-49b4d6e7ebd0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'en cours', '2019-12-12', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `etudiantcritere`
--

CREATE TABLE `etudiantcritere` (
  `ID_ETUDIANT` varchar(64) NOT NULL,
  `ID_CRITERE` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `etudiantcritere`
--

INSERT INTO `etudiantcritere` (`ID_ETUDIANT`, `ID_CRITERE`) VALUES
('4259ac9d-653c-46ac-b09d-cbe6270e411e', '1');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `ID_MESSAGE` varchar(64) NOT NULL,
  `TITRE` varchar(128) NOT NULL,
  `MESSAGE` varchar(512) NOT NULL,
  `VU` tinyint(4) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `HEURE` time NOT NULL,
  `ID_EXPEDITEUR` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

CREATE TABLE `notification` (
  `ID_NOTIFICATION` varchar(64) NOT NULL,
  `TYPE` varchar(128) NOT NULL,
  `MESSAGE` varchar(256) NOT NULL,
  `VUE` tinyint(4) NOT NULL,
  `ID_COORDONNATEUR` varchar(64) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `HEURE` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `offrestage`
--

CREATE TABLE `offrestage` (
  `ID_OFFRE` varchar(64) NOT NULL,
  `STATUT_OFFRE` tinyint(1) NOT NULL,
  `TITRE` varchar(256) NOT NULL,
  `DESCRIPTION` text NOT NULL,
  `DATE_DEBUT` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_FIN` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DUREE_EN_JOURS` int(11) NOT NULL,
  `REMUNERE` tinyint(4) NOT NULL,
  `LIEN_WEB` varchar(256) NOT NULL,
  `LIEN_DOCUMENT` varchar(128) NOT NULL,
  `DATE` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NB_VUES` int(11) NOT NULL,
  `ACTIVE` tinyint(4) NOT NULL,
  `ID_EMPLOYEUR` varchar(64) NOT NULL,
  `FICHIER` longblob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `offrestage`
--

INSERT INTO `offrestage` (`ID_OFFRE`, `STATUT_OFFRE`, `TITRE`, `DESCRIPTION`, `DATE_DEBUT`, `DATE_FIN`, `DUREE_EN_JOURS`, `REMUNERE`, `LIEN_WEB`, `LIEN_DOCUMENT`, `DATE`, `NB_VUES`, `ACTIVE`, `ID_EMPLOYEUR`, `FICHIER`) VALUES
('db2f0dc5-9ad8-4dc8-b1fe-bd403c676753', 1, 'Developpeur Java EE', 'Assiter dans le developpement de notre plateforme en Java EE', '2019-11-24 10:00:00', '2019-11-30 10:00:00', 0, 0, 'snbfjqshdj', '', '2019-12-09 21:19:11', 0, 1, 'ddcc6988-4bcc-4cb1-b756-2fa709727698', ''),
('821fa038-7188-450e-bc93-26a19f04cf7b', 1, 'Developpeur C++', 'Developpement en C++', '2019-11-25 01:29:48', '2019-11-28 10:00:00', 0, 0, 'x', '', '2019-11-25 06:08:52', 0, 1, '1dd808de-19f3-4485-86c2-a3c2f766fae6', ''),
('426f7366-819c-46c4-b2fa-cf0579ce67e1', 1, 'Developpeur ASP.NET', 'Developpement en ASP.NET', '2019-11-25 01:29:43', '2019-11-30 10:00:00', 0, 0, 'x', '', '2019-11-25 06:09:38', 0, 1, '1dd808de-19f3-4485-86c2-a3c2f766fae6', ''),
('cff678b2-1502-4aa6-ad99-b63944cbc5bc', 1, 'Developpeur web Front-end', 'Un programmeur passionne par le web', '2019-12-09 13:47:02', '2019-11-07 10:00:00', 0, 0, 'x', '', '2019-10-30 23:47:05', 0, 1, 'ddcc6988-4bcc-4cb1-b756-2fa709727698', '');

-- --------------------------------------------------------

--
-- Structure de la table `offrestagecritere`
--

CREATE TABLE `offrestagecritere` (
  `ID_OFFRE` varchar(64) NOT NULL,
  `ID_CRITERE` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `offrestagecritere`
--

INSERT INTO `offrestagecritere` (`ID_OFFRE`, `ID_CRITERE`) VALUES
('426f7366-819c-46c4-b2fa-cf0579ce67e1', '1'),
('426f7366-819c-46c4-b2fa-cf0579ce67e1', '3'),
('426f7366-819c-46c4-b2fa-cf0579ce67e1', '4'),
('821fa038-7188-450e-bc93-26a19f04cf7b', '5'),
('db2f0dc5-9ad8-4dc8-b1fe-bd403c676753', '1'),
('db2f0dc5-9ad8-4dc8-b1fe-bd403c676753', '2'),
('db2f0dc5-9ad8-4dc8-b1fe-bd403c676753', '4');

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

CREATE TABLE `professeur` (
  `ID_PROFESSEUR` varchar(64) NOT NULL,
  `VILLE` varchar(64) DEFAULT NULL,
  `ANGLAIS_ECRIT` varchar(32) DEFAULT NULL,
  `ANGLAIS_PARLE` varchar(32) DEFAULT NULL,
  `MOYEN_TRANSPORT` varchar(32) DEFAULT NULL,
  `INFOS_PARTAGEES` varchar(32) DEFAULT NULL,
  `PREFERENCE_SUPERVISION` varchar(64) DEFAULT NULL,
  `DISPO_STAGE_ETE` varchar(8) DEFAULT NULL,
  `PROFIL_CREE` date DEFAULT NULL,
  `PROFIL_MODIFIE` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `professeur`
--

INSERT INTO `professeur` (`ID_PROFESSEUR`, `VILLE`, `ANGLAIS_ECRIT`, `ANGLAIS_PARLE`, `MOYEN_TRANSPORT`, `INFOS_PARTAGEES`, `PREFERENCE_SUPERVISION`, `DISPO_STAGE_ETE`, `PROFIL_CREE`, `PROFIL_MODIFIE`) VALUES
('072e6725-429b-4019-b6f5-3dc70d5655c3', 'Laval', 'Correcte', 'Faible', 'Auto', 'Courriel et tÃ©lÃ©phone', 'Programmation en prioritÃ© puis RÃ©seau', 'Non', '2019-10-27', '2019-11-08'),
('33eddf16-fc10-4af5-a25a-9bb29630f986', 'Montreal', 'Correcte', 'Excellente', 'Auto', 'Courriel seulement', 'Programmation', 'Non', '2019-10-30', '2019-11-28'),
('f56e08ef-733b-4ca9-90e5-a9cd279ef8f2', 'MontrÃ©al', 'Excellente', 'Correcte', 'Auto', 'Courriel et tÃ©lÃ©phone', 'RÃ©seau', 'Non', '2019-10-30', '2019-12-16');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `ID_UTILISATEUR` varchar(64) NOT NULL,
  `COURRIEL` varchar(128) DEFAULT NULL,
  `USERNAME` varchar(128) NOT NULL,
  `MOT_DE_PASSE` varchar(128) NOT NULL,
  `NOM` varchar(64) DEFAULT NULL,
  `PRENOM` varchar(64) DEFAULT NULL,
  `TELEPHONE` varchar(64) DEFAULT NULL,
  `TYPE_UTILISATEUR` varchar(32) NOT NULL,
  `ETAT_COMPTE` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`ID_UTILISATEUR`, `COURRIEL`, `USERNAME`, `MOT_DE_PASSE`, `NOM`, `PRENOM`, `TELEPHONE`, `TYPE_UTILISATEUR`, `ETAT_COMPTE`) VALUES
('5723606a-29bf-4b01-bb06-7e368ce012ef', 'guillaumefongponne@gmail.com', 'guillaume', 'guillaume', 'Fong-Ponne', 'Guillaume', '204-777-4392', 'administrateur', 1),
('7be38f86-916a-4525-98ba-97be67fd5097', 'lia@outlook.com', 'Lia', 'lia', 'Lia', 'Shin', '514-825-1113', 'etudiant', 1),
('ddcc6988-4bcc-4cb1-b756-2fa709727698', 'yuna@gmail.com', 'Yuna', 'yuna', 'Yuna', 'Shin', '514-464-5742', 'employeur', 1),
('072e6725-429b-4019-b6f5-3dc70d5655c3', 'yeji@gmail.com', 'yeji', 'yeji', 'Ali', 'Hwang', '514-123-4567', 'professeur', 1),
('f56e08ef-733b-4ca9-90e5-a9cd279ef8f2', 'glacasse@gmail.com', 'glacasse', 'glacasse', 'Lacasse', 'Gilles', '306-221-3141', 'coordonnateur', 1),
('1dd808de-19f3-4485-86c2-a3c2f766fae6', 'bgates@gmail.com', 'bgates', 'bgates', 'Gates', 'Bill', '416-210-3628', 'employeur', 1),
('33eddf16-fc10-4af5-a25a-9bb29630f986', 'jennie@gmail.com', 'jennie', 'jennie', 'Kim', 'Jennie', '514-456-8542', 'coordonnateur', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurmessage`
--

CREATE TABLE `utilisateurmessage` (
  `ID_MESSAGE` varchar(64) NOT NULL,
  `ID_DESTINATAIRE` varchar(64) NOT NULL,
  `LU` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`ID_ADRESSE`);

--
-- Index pour la table `candidature`
--
ALTER TABLE `candidature`
  ADD PRIMARY KEY (`ID_ETUDIANT`,`ID_OFFRE`);

--
-- Index pour la table `compagnie`
--
ALTER TABLE `compagnie`
  ADD PRIMARY KEY (`ID_COMPAGNIE`);

--
-- Index pour la table `coordonnateur`
--
ALTER TABLE `coordonnateur`
  ADD PRIMARY KEY (`ID_COORDONNATEUR`);

--
-- Index pour la table `critere`
--
ALTER TABLE `critere`
  ADD PRIMARY KEY (`ID_CRITERE`);

--
-- Index pour la table `cv`
--
ALTER TABLE `cv`
  ADD PRIMARY KEY (`ID_CV`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`ID_DOCUMENT`);

--
-- Index pour la table `employeur`
--
ALTER TABLE `employeur`
  ADD PRIMARY KEY (`ID_EMPLOYEUR`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`ID_ETUDIANT`);

--
-- Index pour la table `etudiantcritere`
--
ALTER TABLE `etudiantcritere`
  ADD PRIMARY KEY (`ID_ETUDIANT`,`ID_CRITERE`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`ID_MESSAGE`);

--
-- Index pour la table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`ID_NOTIFICATION`);

--
-- Index pour la table `offrestage`
--
ALTER TABLE `offrestage`
  ADD PRIMARY KEY (`ID_OFFRE`);

--
-- Index pour la table `offrestagecritere`
--
ALTER TABLE `offrestagecritere`
  ADD PRIMARY KEY (`ID_OFFRE`,`ID_CRITERE`);

--
-- Index pour la table `professeur`
--
ALTER TABLE `professeur`
  ADD PRIMARY KEY (`ID_PROFESSEUR`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`ID_UTILISATEUR`),
  ADD UNIQUE KEY `USERNAME` (`USERNAME`),
  ADD UNIQUE KEY `COURRIEL` (`COURRIEL`);

--
-- Index pour la table `utilisateurmessage`
--
ALTER TABLE `utilisateurmessage`
  ADD PRIMARY KEY (`ID_MESSAGE`,`ID_DESTINATAIRE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
