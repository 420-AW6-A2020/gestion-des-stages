<%-- 
    Document   : liste-assignations-coordonnateur
    Created on : 20 nov. 2019, 23:42:10
    Author     : tchui
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<%@page import="com.util.Util"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>

<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="empdao" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="empdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="adao" class="com.stageo.dao.AdresseDAO">
    <jsp:setProperty name="adao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCandidature" scope="page" class="com.stageo.dao.CandidatureDAO">
    <jsp:setProperty name="daoCandidature" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="pdao" class="com.stageo.dao.ProfesseurDAO">
    <jsp:setProperty name="pdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="stagedao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="stagedao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" scope="page" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>
<c:set  var="listeProfesseur" value="${pdao.findAll()}"/>
<c:set  var="listeCandidatures" value="${daoCandidature.findByStatut('Confirmer')}"/>
<c:set var="listeUser" value="${dao.findAll()}"/>
<jsp:include page="/WEB-INF/vue/modals/modalDetailsCandidature.jsp"></jsp:include>
    <section class="container py-5">
        <article class="row">
            <div class="col-12 col-lg-6">
                <h1>Liste assignations</h1>
            </div>
            <div class="col-12 col-lg-6">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text"><i class="fas fa-search"></i></label>
                    </div>
                    <input id="inputRecherche" onkeyup="rechercherAssignation()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
                </div>
            </div>
        </article>

        <article class="row">
            <div class="col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text">
                            <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                            <ul class="dropdown-menu" id="liste">
                            <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                </li>
                            </c:forEach>    
                        </ul>
                    </label>
                </div>
                <!-- Conteneur de comperence -->
                <div class="col-sm-8" >
                    <div id="conteneurCompetences">

                    </div>
                </div>
            </div>
        </div>
    </article>

    <article class="row">
        <div class="col-12">
            <table id="table" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'table')">Nom</th>
                        <th onclick="sortTable(1, 'table')">Prénom</th>
                        <th onclick="sortTable(2, 'table')">Courriel</th>
                        <th onclick="sortTable(3, 'table')">Titre du stage </th>
                        <th>Superviseur <a class="sortBy" href="#" onclick="sortTable('table', 4, 'asc')"><span class="fas fa-sort"></span></a></th>
                        <th>Informations partagées</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeCandidatures}" var="candidature">
                        <c:set var="etudiant" value="${edao.read(candidature.idEtudiant)}"/>
                        <c:set var="stage" value="${stagedao.findById(candidature.idOffre)}"/>
                        <c:set var="employeur" value="${empdao.read(stage.idEmployeur)}"/>
                        <c:set var="compagnie" value="${cdao.read(employeur.compagnie.idCompagnie)}"/>
                        <c:set var="adresse" value="${adao.findByID(compagnie.adresse.idAdresse)}"/>
                        <c:if test="${candidature.idProfesseur != null }">
                            <c:set var="superviseur" value="${pdao.read(candidature.idProfesseur)}"/>
                        </c:if>
                        <tr>
                            <td>${etudiant.nom}</td>
                            <td>${etudiant.prenom}</td>
                            <td hidden>${etudiant.profilEtudes}</td>
                            <td>${etudiant.email}</td>
                            <td hidden>${etudiant.telephone}</td>
                            <td>${stage.titre}</td>
                            <td><c:out value="${superviseur.nom}" default="Aucun enseignant assigne"/></td>
                            <td hidden>${superviseur.telephone}</td>
                            <td hidden>${compagnie.nom}</td>
                            <td hidden>${employeur.nom}</td>
                            <td hidden>${compagnie.telEntreprise}</td>
                            <td hidden>${compagnie.pageWeb}</td>
                            <td hidden>${compagnie.lienGoogleMaps}</td>
                            <td hidden><fmt:formatDate value="${stage.dateDebut}" pattern="MM/dd/yyyy"/></td>
                            <td hidden><fmt:formatDate value="${stage.dateFin}" pattern="MM/dd/yyyy"/></td>
                            <td hidden>${compagnie.kilometrage}</td>
                            <c:if test="${stage.remunere == 0}"><c:set var="remuneration" value="Non" scope="page"/></c:if>
                            <td hidden><c:out value="${remuneration}" default="Oui"/></td>
                            <c:remove var="remuneration" scope="page"/>
                            <td hidden>${adresse.adresseLigne1}</td>
                            <td hidden>${adresse.adresseLigne2}</td>
                            <td hidden>${adresse.ville}</td>
                            <td hidden>${adresse.codePostal}</td>
                            <td hidden>${adresse.province}</td>
                            <td  hidden id="etudiant">${candidature.idEtudiant}</td>
                            <td  hidden id="offre">${candidature.idOffre}</td>
                            <td hidden>${superviseur.email}</td>
                            <td>${Util.toUTF8(superviseur.infosPartagees)}</td>
                            <c:remove var="superviseur"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
</section>
<form>
    <div class="modal fade top" id="modalListeProfesseur" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-center modal-notify modal-info" role="document">
            <div class="modal-content">
                <div class="modal-header deep-purple darken-2 justify-content-between">
                    <h1 class="white-text">Liste professeur</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class='white-text'>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="x" items="${listeProfesseur}">
                                <c:set var="professeur" value="${dao.read(x.idUtilisateur)}" />
                                <tr style="border-top: 0px;">
                                    <td>${professeur.nom}</td>
                                    <td>${professeur.prenom}</td>
                                    <td class="d-flex justify-content-end">
                                        <button type="submit" class="btn btn-outline-primary" onclick="assigner('${professeur.idUtilisateur}')">
                                            Assigner
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                        <input type="hidden" name="tache" value="effectuerAssignationAction"/>
                        <input type="hidden" id="idProfesseur" name="idProfesseur" value=""/>
                        <input type="hidden" id="Offre" name="Offre"/>
                        <input type="hidden" id="Etudiant" name="Etudiant"/>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script>
                                            function assigner(id) {
                                                document.getElementById("idProfesseur").value = id;
                                                document.getElementById("idEtudiant").value = document.getElementById("etudiant").innerHTML;
                                                document.getElementById("idOffre").value = document.getElementById("offre").innerHTML;
                                            }

                                            var nbComp = 0;

                                            var table = document.getElementById('table');
                                            var listeCriteres;

                                            for (var i = 1; i < table.rows.length; i++)
                                            {
                                                table.rows[i].onclick = function ()
                                                {

                                                    //rIndex = this.rowIndex;
                                                    document.getElementById("nom").value = this.cells[0].innerHTML;
                                                    document.getElementById("prenom").value = this.cells[1].innerHTML;
                                                    document.getElementById("domaine").value = this.cells[2].innerHTML;
                                                    document.getElementById("email").value = this.cells[3].innerHTML;
                                                    document.getElementById("tel").value = this.cells[4].innerHTML;
                                                    document.getElementById("titreStage").value = this.cells[5].innerHTML;
                                                    document.getElementById("superviseur").value = this.cells[6].innerHTML;
                                                    document.getElementById("telSuperviseur").value = this.cells[7].innerHTML;
                                                    document.getElementById("nomEntreprise").value = this.cells[8].innerHTML;
                                                    document.getElementById("personneRessource").value = this.cells[9].innerHTML;
                                                    document.getElementById("telEntreprise").value = this.cells[10].innerHTML;
                                                    document.getElementById("pageWeb").value = this.cells[11].innerHTML;
                                                    document.getElementById("lienGoogleMaps").value = this.cells[12].innerHTML;
                                                    document.getElementById("dateDebut").value = this.cells[13].innerHTML;
                                                    document.getElementById("dateFin").value = this.cells[14].innerHTML;
                                                    document.getElementById("kilometrage").value = this.cells[15].innerHTML;
                                                    document.getElementById("remuneration").value = this.cells[16].innerHTML;
                                                    document.getElementById("adresseLigne1").value = this.cells[17].innerHTML;
                                                    document.getElementById("adresseLigne2").value = this.cells[18].innerHTML;
                                                    document.getElementById("ville").value = this.cells[19].innerHTML;
                                                    document.getElementById("codePostal").value = this.cells[20].innerHTML;
                                                    document.getElementById("province").value = this.cells[21].innerHTML;
                                                    document.getElementById("idEtudiant").value = this.cells[22].innerHTML;
                                                    document.getElementById("idOffre").value = this.cells[23].innerHTML;
                                                    document.getElementById("Etudiant").value = this.cells[22].innerHTML;
                                                    document.getElementById("Offre").value = this.cells[23].innerHTML;
                                                    document.getElementById("courrielSuperviseur").value = this.cells[24].innerHTML;
                                                    $(".Catpill").css({"display": "inline"});

                                                    $('#modalDetailsCandidature').modal('show');

                                                };
                                            }

                                            function replaceSpec(Texte) {
                                                var TabSpec = {"à": "a", "á": "a", "â": "a", "ã": "a", "ä": "a", "å": "a", "ò": "o", "ó": "o", "ô": "o", "õ": "o", "ö": "o", "ø": "o", "è": "e", "é": "e", "ê": "e", "ë": "e", "ç": "c", "ì": "i", "í": "i", "î": "i", "ï": "i", "ù": "u", "ú": "u", "û": "u", "ü": "u", "ÿ": "y", "ñ": "n", "-": " ", "_": " "},
                                                        reg = /[àáâãäåòóôõöøèéêëçìíîïùúûüÿñ_-]/gi;

                                                return Texte.replace(reg, function () {
                                                    return TabSpec[arguments[0].toLowerCase()];
                                                }).toLowerCase();
                                            }
                                            function sortTable(tid, col, ord) {
                                                var mybody = $("#" + tid).children('tbody'),
                                                        lines = mybody.children('tr'),
                                                        sorter = [],
                                                        i = -1,
                                                        j = -1;

                                                while (lines[++i]) {
                                                    sorter.push([lines.eq(i), lines.eq(i).children('td').eq(col).text()])
                                                }

                                                if (ord == 'asc') {
                                                    sorter.sort(function (a, b) {
                                                        var c1 = replaceSpec(a[1]),
                                                                c2 = replaceSpec(b[1]);

                                                        if (c1 > c2) {
                                                            return 1;
                                                        } else if (c1 < c2) {
                                                            return -1;
                                                        } else {
                                                            return 0;
                                                        }
                                                    });
                                                } else if (ord == 'desc') {
                                                    sorter.sort(function (a, b) {
                                                        var c1 = replaceSpec(a[1]),
                                                                c2 = replaceSpec(b[1]);

                                                        if (c1 > c2) {
                                                            return -1;
                                                        } else if (c1 < c2) {
                                                            return 1;
                                                        } else {
                                                            return 0;
                                                        }
                                                    });
                                                } else if (ord == 'nasc') {
                                                    sorter.sort(function (a, b) {
                                                        return(a[1] - b[1]);
                                                    });
                                                } else if (ord == 'ndesc') {
                                                    sorter.sort(function (a, b) {
                                                        return(b[1] - a[1]);
                                                    });
                                                } else if (ord == '?asc') {
                                                    sorter.sort(function (a, b) {
                                                        var x = parseInt(a[1], 10),
                                                                y = parseInt(b[1], 10),
                                                                c1 = replaceSpec(a[1]),
                                                                c2 = replaceSpec(b[1]);

                                                        if (isNaN(x) || isNaN(y)) {
                                                            if (c1 > c2) {
                                                                return 1;
                                                            } else if (c1 < c2) {
                                                                return -1;
                                                            } else {
                                                                return 0;
                                                            }
                                                        } else {
                                                            return(a[1] - b[1]);
                                                        }
                                                    });
                                                } else if (ord == '?desc') {
                                                    sorter.sort(function (a, b) {
                                                        var x = parseInt(a[1], 10),
                                                                y = parseInt(b[1], 10),
                                                                c1 = replaceSpec(a[1]),
                                                                c2 = replaceSpec(b[1]);

                                                        if (isNaN(x) || isNaN(y)) {
                                                            if (c1 > c2) {
                                                                return -1;
                                                            } else if (c1 < c2) {
                                                                return 1;
                                                            } else {
                                                                return 0;
                                                            }
                                                        } else {
                                                            return(b[1] - a[1]);
                                                        }
                                                    });
                                                }

                                                while (sorter[++j]) {
                                                    mybody.append(sorter[j][0]);
                                                }
                                            }
                                            jQuery(document).ready(function () {
                                                $(".elementCritere").click(function (event) {
                                                    document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
                                                    $(".competence").addClass("alert alert-info ")
                                                            .css("margin", "2%");
                                                    nbComp++;
                                                });

                                            });

 function rechercherAssignation() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td3 = tr[i].getElementsByTagName("td")[3].textContent.toUpperCase();
        var td5 = tr[i].getElementsByTagName("td")[5].textContent.toUpperCase();
        var td6 = tr[i].getElementsByTagName("td")[6].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td3.indexOf(filter) > -1 || td5.indexOf(filter) > -1 || td6.indexOf(filter) > -1 ) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }  

</script>

<style>
    th{
        cursor:pointer;
    }
</style>
