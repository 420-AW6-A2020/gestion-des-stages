
<%-- 
    Document   : profilEtudiant
    Created on : 2018-10-25, 10:17:32
    Author     : JP
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page import="com.stageo.model.Critere"%>
<%@page import="com.stageo.dao.CritereDAO"%>
<%@page import="com.stageo.model.ListeChoix"%>
<%@page import="com.util.Util"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<jsp:useBean id="daoEtudiant" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="daoEtudiant" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="etudiant" scope="page" value="${daoEtudiant.read(sessionScope.connecte)}"/>

<jsp:include page="modals/modal-modif-compte.jsp"></jsp:include>

<div class="jumbotron jumbotron-fluid stylish-color white-text">
    <section class="container">
        <article class="row justify-content-center">
            <div class="col-lg-6 col-12 text-center">
                <h1><i class="fas fa-user-circle" style="font-size:150px"></i></h1>
                <h3>${Util.toUTF8(etudiant.prenom)} ${Util.toUTF8(etudiant.nom)}</h3>
                <h4>@${etudiant.nomUtilisateur}</h4>
                <h5>${etudiant.email}</h5>
                <h5>${fn:toUpperCase(etudiant.role)}</h5>
                <p>
                    Membre depuis : ${etudiant.profilCree}
                    <br>
                    Dernière modification : ${etudiant.profilModifie}
                </p>
                <a data-toggle="modal" href="#monModalMotDePasse" class="btn btn-success btn-sm" role="button">Changer mon mot de passe</a>
                <a data-toggle="modal" href="#monModalNomUtilisateur" class="btn btn-success btn-sm" role="button">Changer mon nom d'utilisateur</a>
        </article>
    </section>
</div>

<section class="container">
    <form action="*.do" method="post" class="needs-validation" novalidate>
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Informations générales</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <section class="container-fluid p-0">
                    <article class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nom</label>
                                <input type="text" class="form-control" name="nom" value="${Util.toUTF8(etudiant.nom)}" required>
                                <div class="invalid-feedback">Veuillez entrer votre nom.</div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Prénom</label>
                                <input type="text" class="form-control" name="prenom" value="${Util.toUTF8(etudiant.prenom)}"required>
                                <div class="invalid-feedback">Veuillez entrer votre pr&eacute;nom.</div>
                            </div>
                        </div>
                    </article>
                </section>

                <div class="form-group">
                    <label for="">Adresse courriel</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : abcd@nom-de-domaine.com"></label>                    
                    <input type="email" class="form-control" name="email" value="${etudiant.email}" required>
                    <div class="invalid-feedback">Veuillez entrer une adresse courriel valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : XXX-XXX-XXXX"></label>                    
                    <input type="tel" class="form-control" name="tel" value="${etudiant.telephone}" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                    <div class="invalid-feedback">Veuillez entrer un num&eacute;ro de t&eacute;l&eacute;phone valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Ville</label>
                    <input type="text" class="form-control" name="ville" value="${Util.toUTF8(etudiant.ville)}" required>
                    <div class="invalid-feedback">Veuillez entrer votre ville.</div>
                </div>

            </div>
        </article>

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Profil académique</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Profil études</label>
                    <select name='profilEtudes' class='form-control' required>
                        <option value=''>Choisir un profil...</option>
                        <c:forEach items="${ListeChoix.getListeProfilEtudes()}" var="profil">
                            <c:choose>
                                <c:when test="${profil == Util.toUTF8(etudiant.profilEtudes)}">
                                    <option value='${profil}' selected>${profil}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value='${profil}'>${profil}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez choisir un profil d&CloseCurlyQuote;&eacute;tude.</div>
                </div>
                <div class="form-group">
                    <label for="">Session du stage</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : A-année ou H-année"></label>                    
                    <input type="text" class="form-control" name="stageSession" value="${etudiant.stageSession}" required>
                    <div class="invalid-feedback">Veuillez entrer votre session.</div>
                </div>
                <div class="form-group">
                    <label for="">Anglais écrit</label>
                    <select class="form-control validate" name="anglaisEcrit" required>
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeChoixAnglais()}" var="choix">
                            <c:choose>
                                <c:when test="${etudiant.anglaisEcrit == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez s&eacute;lectionner votre niveau en aglais &eacute;crit.</div>
                </div>
                <div class="form-group">
                    <label for="">Anglais parlé</label>
                    <select class="form-control" name="anglaisParle" required>
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeChoixAnglais()}" var="choix">
                            <c:choose>
                                <c:when test="${etudiant.anglaisParle== choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez s&eacute;lectionner votre niveau en aglais parl&eacute;.</div>
                </div>
                <div class="form-group">
                    <label for="">Numéro DA</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="7 chiffres du numéro d'admission"></label>                    
                    <input type="text" class="form-control" name="numeroDA" value="${etudiant.numeroDA}" required>
                    <div class="invalid-feedback">Veuillez entrer votre num&eacute;ro DA.</div>
                </div>
                <div class="form-group">
                    <label for="">Login 365</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="numeroDA@crosemont.qc.ca"></label>                    
                    <input type="email" class="form-control" name="login365" value="${etudiant.login365}" required>
                    <div class="invalid-feedback">Veuillez entrer votre login office 365.</div>
                </div>

            </div>
        </article>    

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Préférences Stages</strong></h1>
            </div>
            <div class="col-12 col-lg-6">
                <div class="form-group">
                    <label for="">Démarches en cours</label>
                    <select class="form-control" name="demarcheEnCours" required>
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeDemarches()}" var="choix">
                            <c:choose>
                                <c:when test="${etudiant.demarcheEnCours == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez sp&eacute;cifier si vous avez des d&eacute;marches en cours.</div>
                </div>
                <div class="form-group">
                    <label for="">Permis de conduire valide</label>
                    <select class="form-control" name="permisConduire" required>
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeOuiNon()}" var="choix">
                            <c:choose>
                                <c:when test="${etudiant.permisConduire == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez sp&eacute;cifier si vous poss&eacute;der un permis de conduire valide.</div>
                </div>
                <div class="form-group">
                    <label for="">Moyens de transport</label>
                    <select class="form-control" name="moyenTransport" required>
                        <option value="">Choisir..</option>
                        <c:forEach items="${ListeChoix.getListeMoyenTransport()}" var="choix">
                            <c:choose>
                                <c:when test="${etudiant.moyenTransport == choix}">
                                    <option value="${choix}" selected>${choix}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${choix}">${choix}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                    <div class="invalid-feedback">Veuillez sp&eacute;cifier votre moyen de transport.</div>
                </div>
                <div class="form-group">
                    <label for="">Préférences</label>
                    <textarea class="form-control" rows="5" name="preferences">${Util.toUTF8(etudiant.preferences)}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Principales compétences</label>
                </div>
                <c:forEach items="${listeCriteres}" var="critere">
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <c:set var="trouve" value="false"></c:set>
                            <c:forEach items="${etudiant.listeCritere}" var="critereEtudiant">
                                <c:if test="${critereEtudiant.idCritere.equals(critere.idCritere)}">
                                    <input type="checkbox" class="form-check-input" name="${critere.nom}Check" value="${critere.idCritere}" checked>${critere.nom}
                                    <c:set var="trouve" value="true"></c:set>
                                </c:if>
                            </c:forEach>
                            <c:if test="${trouve == false}">
                                <input type="checkbox" class="form-check-input" name="${critere.nom}Check" value="${critere.idCritere}">${critere.nom}
                            </c:if>

                        </label>
                    </div>
                </c:forEach>


                <!--                <table  id="competence" class="table table-hover bg-light">
                                    <thead>
                                        <tr>
                                            <th scope="col">Critère <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                            <th scope="col">Maitrîse <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                
                <%--<c:forEach items="${listeCriteres}" var="critere"> 
                    <tr>
                        <td name="critereNom">${critere.nom}
                        </td>
                        <td>
                            <input name="${critere.idCritere}" class="checkbox form-control mr-auto" type="checkbox" id="${critere.idCritere}">
                        </td>
                    </tr>
                </c:forEach> --%>

            </tbody>
        </table>-->
            </div>
        </article>

        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">

            </div>

            <div class="col-12 col-lg-6 d-flex justify-content-between">
                <button type="submit" class="btn btn-success" name="tache" value="effectuerModificationUtilisateur">
                    Enregistrer les modifications
                </button>
                <button type="reset" class="btn btn-light">Annuler</button>
            </div>

        </article>
    </form>
</section>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script>
//    $('#info').popover({trigger: "hover", placement: "right", content: "Profil consulté : 104 fois"});

    $('document').ready(function () {


        $(".checkbox").each(function (index) {
            this.checked = (${etudiant.getListeCritere()}.includes(Number(this.id)));
        });
    });

</script>
