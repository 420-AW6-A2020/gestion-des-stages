<%-- 
    Document   : modalConfirmationDelete
    Created on : Dec 8, 2019, 5:12:24 PM
    Author     : guill
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>

<!--Modal: modalConfirmation -->
<div class="modal fade" id="modalConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-info" role="document">
    <!--Content-->
        <div class="modal-content text-center">
      <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">�tes-vous s�r ?</p>
            </div>

            <!--Body-->
            <div class="modal-body">
                <i class="fas fa-question-circle fa-4x animated rotateIn"></i>
                <p id="msgModalConfirmation" class='text-center font-weight-bold'></p>
            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <form id="formModalConfirmation">
                    <input type="hidden" id="tacheConfirmation" name="tache" value="">
                    <input type="hidden" id="idOffreConfirmation" name="idOffre">
                    <input type="hidden" id="idEtudiantConfirmation" name="idEtudiant">
                    <input type="hidden" id="idCandidatureConfirmation" name="idCandidature">
                    
                    <input type="hidden" name="titre" class="form-control" id="titreConfirmation">
                    <textarea name="desc" class="md-textarea form-control" id="descriptionConfirmation" hidden>default</textarea>
                    <input value="default" type="hidden" name="lienWeb" class="form-control" id="lienWebConfirmation" >
                    <input type="hidden" name="date_debut" class="form-control" id="dateDebutConfirmation" >
                    <input type="hidden" name="date_fin" class="form-control" id="dateFinConfirmation" >
                    <input value="default" type="hidden" name="remuneration" class="form-control" id="remunerationConfirmation" >
                    <input value="default" type="hidden" name="fichier" class="form-control-file" id="fichierConfirmation">
                    <c:forEach items="${listeCriteres}" var="critere">
                        <div id='listeCriteresConfirmation' class="custom-control custom-checkbox custom-control-inline pr-5" hidden>
                            <input name="${critere.idCritere}" class="custom-control-input mr-auto critereConfirmation" type="checkbox" id="${critere.idCritere}Confirmation">
                            <label class="custom-control-label" for="${critere.idCritere}" hidden>${critere.nom}</label>
                        </div>
                    </c:forEach>
                    
                    <button type="submit" class="btn btn-outline-info">Oui</button>
                </form>
                
                <a type="button" class="btn btn-info waves-effect" data-dismiss="modal">Non</a>
            </div>
        </div>
    <!--/.Content-->
    </div>
</div>
<!--Modal: modalConfirmation-->
