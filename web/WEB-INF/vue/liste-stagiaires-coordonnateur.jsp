<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.*"%>
<%@page import="com.util.Util"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeUser" value="${dao.findAll()}"/>
<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="edao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<jsp:include page="/WEB-INF/vue/modals/modalDetailsEtudiants.jsp"></jsp:include>
<jsp:include page="/WEB-INF/vue/modals/modalDetailsOffres.jsp"></jsp:include>

<section class="container py-5">
    <article class="row">
        <div class="col-12 col-lg-6">
            <h1>Rechercher Stagiaire</h1>
        </div>
        <div class="col-12 col-lg-6">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text"><i class="fas fa-search"></i></label>
                </div>
                <input id="inputRecherche" onkeyup="rechercherEtudiant()" type="text" class="form-control" placeholder="Recherche par mot-clef...">
            </div>
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text">
                        <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                        <ul class="dropdown-menu" id="liste">
                            <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                </li>
                            </c:forEach>    
                        </ul>
                    </label>
                </div>
                <!-- Conteneur de comperence -->
                <div class="col-sm-8" >
                    <div id="conteneurCompetences">

                    </div>
                </div>
            </div>
        </div>
    </article>
    
    <article class="row">
        <div class="col-12">
            <table id="table" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'table')">Nom <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th onclick="sortTable(1, 'table')">Prénom <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                        <th onclick="sortTable(2, 'table')">Courriel <span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${listeUser}" var="user">
                        <c:if test = "${user.role == 'etudiant'}">
                            <c:set var="etudiant" value="${edao.read(user.idUtilisateur)}"></c:set>
                                <tr>
                                    <td>${user.nom}</td>
                                    <td>${user.prenom}</td>
                                    <td>${user.email}</td>
                                    <td hidden>${user.idUtilisateur}</td>
                                    <td hidden>${user.telephone}</td>
                                    <td hidden>${Util.toUTF8(etudiant.ville)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.profilEtudes)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.stageSession)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.anglaisEcrit)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.anglaisParle)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.permisConduire)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.demarcheEnCours)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.login365)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.numeroDA)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.moyenTransport)}</td>
                                    <td hidden>${Util.toUTF8(etudiant.preferences)}</td>
                                    
                                    <td hidden name="crit">
                                        <c:forEach  items="${etudiant.listeCritere}" var="crit">
                                            ${crit.nom},
                                        </c:forEach>
                                    </td>
                                        
                                    
                                </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
</section>

<script>
    var nbComp = 0;

    var table = document.getElementById('table');
    var listeCriteres;

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[1].innerHTML;
            document.getElementById("nom").value = this.cells[0].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("idUser").value = this.cells[3].innerHTML;
            document.getElementById("tel").value = this.cells[4].innerHTML;
            document.getElementById("ville").value = this.cells[5].innerHTML;
            document.getElementById("profilEtudes").value = this.cells[6].innerHTML;
            document.getElementById("stageSession").value = this.cells[7].innerHTML;
            document.getElementById("anglaisEcrit").value = this.cells[8].innerHTML;
            document.getElementById("anglaisParle").value = this.cells[9].innerHTML;
            document.getElementById("permisConduire").value = this.cells[10].innerHTML;
            document.getElementById("demarcheEnCours").value = this.cells[11].innerHTML;
            document.getElementById("login365").value = this.cells[12].innerHTML;
            document.getElementById("numeroDA").value = this.cells[13].innerHTML;
            document.getElementById("moyenTransport").value = this.cells[14].innerHTML;
            document.getElementById("preferences").innerHTML = this.cells[15].innerHTML;
            
            listeCriteres = this.cells[16].innerHTML;
            listeCriteres = listeCriteres.trim();
            listeCriteres = listeCriteres.split(",");
            console.log(listeCriteres);
            document.getElementById("criteresStageContent").innerHTML = "";
            for (var i = 0; i < listeCriteres.length; i++){
                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + listeCriteres[i] + "</span></h2>";
            }
            
//            $('#' + this.cells[4].innerHTML).children("td[name='crit']").each(function (index) {
//                document.getElementById("criteresStageContent").innerHTML += "<h2 class='Catpill'><span class='badge badge-pill mb-1'>" + $(this).text() + "</span></h2>";
//            });
            $(".Catpill").css({"display": "inline"});
            
            $('#modalDetailsEtudiants').modal('show');

        };
    }
    
    $(document).ready(function () {
        $(".elementCritere").click(function (event) {
            document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
            $(".competence").addClass("alert alert-info ")
                    .css("margin", "2%");
            nbComp++;
        });

    });

    function rechercherEtudiant() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.getElementsByTagName("tr");
    
    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 ) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }
    
</script>

<style>
    th{
        cursor:pointer;
    }
</style>    
