<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoDocument" scope="page" class="com.stageo.dao.DocumentDAO">
    <jsp:setProperty name="daoDocument" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeDocument" value="${daoDocument.findAll()}"/>

<div class="container py-5" >



    <div class="row" >

        <div class="col-md-7">

            <div class="card">
                <div class="card-header titreProfil">
                    <h1 class="card-title ">Ajouter Document</h1>
                </div>
                <div class="card-body">

                    <form action="*.do?tache=ajouterDocument" method="post" enctype="multipart/form-data"> 


                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Titre :</label>
                            </div>
                            <input type="text"  name="titre" class="form-control" id="titre" value="1"  placeholder="Titre" required>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Type :</label>
                            </div>
                            <input type="text"  name="type" class="form-control" id="type" value="1" placeholder="Type" required>
                        </div>     
                        <small  class="form-text text-muted"> Veuillez choisir un fichier PDF</small>
                        <div class="input-group mb-3">

                            <input type="text" class="form-control" disabled>

                            <div class="input-group-append ">
                                <label  class="input-group-text btn btn-primary ml-auto ">
                                    <span class="">
                                        Document
                                        <input type="file" class="form-control-file"  hidden="true" accept=".pdf" id="fichier" name="fichier" required>
                                    </span>
                                </label>
                            </div>


                        </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="tache" value="ajouterDocument">
                    <button type="submit" class="btn btn-block my-btn-outline-primary">Ajouter un document</button>
                    </form>
                </div>
            </div>

        </div>
            <div class="col-md-5" >
                 <div class="card">
                <div class="card-header titreProfil2">
                    <h1>Documents</h1>
                </div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Nom</th>
                                <th scope="col">Date</th>
                                <th scope="col">Vues</th>
                                <th scope="col">Consulter</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listeDocument}" var="documents"> 
                                 <fmt:formatDate var="dateDoc" value="${documents.date}" pattern="yyyy-MM-dd" />
                                <tr >
                                    <td>${documents.titre}</td>
                                    <td hidden>${documents.type}</td>
                                    <td>${dateDoc}</td>
                                    <td>${documents.nbVues}</td>
                                    <td><a target="Télécharger" href="?tache=lireDocument&id=${documents.idDocument}"><i class="fas fa-download titreProfil2"></i></a></li></td>
                                </tr>
                            </c:forEach> 
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>
    </div>