
<%-- 
    Document   : profilEtudiant
    Created on : 2018-10-25, 10:17:32
    Author     : JP
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="com.stageo.jdbc.Connexion"%>
<%@page import="com.stageo.jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.dao.UtilisateurDAO"%>
<%@page import="com.util.Util"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>

<jsp:useBean id="connexion" class="com.stageo.jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoCritere" scope="page" class="com.stageo.dao.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>
<c:set var="admin" scope="page" value="${dao.read(sessionScope.connecte)}"/>

<div class="jumbotron jumbotron-fluid stylish-color white-text">
    <section class="container">
        <article class="row justify-content-center">
            <div class="col-lg-6 col-12 text-center">
                <h1><i class="fas fa-user-circle" style="font-size:150px"></i></h1>
                <h3>${Util.toUTF8(admin.prenom)} ${Util.toUTF8(admin.nom)}</h3>
                <h4>@${admin.nomUtilisateur}</h4>
                <h5>${admin.email}</h5>
                <h5>${fn:toUpperCase(admin.role)}</h5>

                <a data-toggle="modal" href="#monModalMotDePasse" class="btn btn-success btn-sm" role="button">Changer mon mot de passe</a>
                <a data-toggle="modal" href="#monModalNomUtilisateur" class="btn btn-success btn-sm" role="button">Changer mon nom d'utilisateur</a>
            </div>
        </article>
    </section>
</div>
                
<section class="container">
    <form action="*.do" method="post">
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                <h1><strong>Informations générales</strong></h1>
            </div>

            <div class="col-12 col-lg-6">
                <section class="container-fluid p-0">
                    <article class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Nom</label>
                                <input type="text" class="form-control" name="nom" value="${Util.toUTF8(admin.nom)}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Prénom</label>
                                <input type="text" class="form-control" name="prenom" value="${Util.toUTF8(admin.prenom)}">
                            </div>
                        </div>
                    </article>
                </section>
                
                <div class="form-group">
                    <label for="">Adresse courriel</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : abcd@nom-de-domaine.com"></label>
                    <input type="email" class="form-control" name="email" value="${admin.email}">
                </div>
                <div class="form-group">
                    <label for="">Numéro de téléphone</label>
                    <label i class="fas fa-question-circle" for="" data-toggle="tooltip" data-placement="right" title="Format : XXX-XXX-XXXX"></label>
                    <input type="tel" class="form-control" name="tel" value="${admin.telephone}" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}">
                </div>
                
            </div>
        </article>
        <article class="row border border-dark border-right-0 border-bottom-0 border-left-0 py-5">
            <div class="col-12 col-lg-6">
                
            </div>
            
            <div class="col-12 col-lg-6 d-flex justify-content-between">
                <button type="submit" class="btn btn-success" name="tache" value="effectuerModificationUtilisateur">
                    Enregistrer les modifications
                </button>
                <button type="submit" class="btn btn-light" name="tache" value="afficherPageProfil">Annuler</button>
            </div>
            
        </article>
    </form>
</section>

<div class="container">
    <div class="row">
        <div class="col-12  mx-auto">
            <div class="card">
                <div class="card-header">
                    <div class="card-title titreProfil">
                        <h1>Espace coordonnateur</h1>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-subtitle mb-3"><h4>Gestion des compétences:</h4>
                    </div>


                    <form action="*.do?tache=ajoutCritere" method = "post" >
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Nouvelle Compétence</label>
                                </div>
                                <input type="text"  name="critere" class="form-control" id="critere"  placeholder="Critère">
                                <button type="submit" class="btn my-btn-outline-primary my-2 my-sm-0"> Ajouter</button>
                            </div>

                    </form>
                    <div class="table-responsive">

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td >Critères <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                                    <td>Étudiants <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                                    <td>Requises <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                                    <td>Supprimer <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listeCriteres}" var="critere"> 
                                    <tr>
                                        <td>${critere.nom}</td>
                                        <td>4</td>
                                        <td>4</td>
                                        <td>
                                            <form action="*.do?tache=supprimerCritere" method = "post">

                                                <input type="hidden" value="${critere.idCritere}" name="suppressionCrit" class="form-control" id="suppressionCrit">
                                                <button type="submit" class="my-btn-outline-primary"><a><i class="fas fa-trash-alt"></i></a></button>
                                            </form>
                                        </td>
                                    </tr>
                                </c:forEach> 

                            </tbody>
                        </table>
                    </div>
                </div>    
            </div> 
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 mx-auto my-5">
            <div class="card">
                <div class="card-header">
                    <div class="card-title titreProfil">
                        <h1>Espace administrateur</h1>
                    </div>
                </div>
                <div class="card-body">
                    <div class="card-subtitle mb-3">
                        <h4>Ouverture de compte</h4>
                    </div>
                    
                    <div class="card-text">
                        <form action="*.do?tache=afficherPageInscription" method="post">
                            <button type="submit" class='btn btn-info'>Créer un compte</button>
                        </form>
                    </div>
                </div>    
            </div> 
        </div>
    </div>
</div>