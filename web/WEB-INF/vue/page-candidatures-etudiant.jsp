<%-- 
    Document   : page-candidatures-etudiant
    Created on : 2018-12-07, 01:23:06
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<%@page import="com.stageo.model.*"%>
<jsp:useBean id="dao" class="com.stageo.dao.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="etudao" class="com.stageo.dao.EtudiantDAO">
    <jsp:setProperty name="etudao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" class="com.stageo.dao.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="odao" class="com.stageo.dao.StageDAO">
    <jsp:setProperty name="odao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.dao.EmployeurDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>
<c:set var="etudiant" scope="page" value="${etudao.read(sessionScope.connecte)}"/>

<section class="container py-5">
    
        <div class="col-12">
            <h1>Candidatures en cours</h1>
        </div>
            <div class='col-lg-6 col-12'>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text"><i class="fas fa-search"></i></label>
                    </div>
                    <input id="inputRecherche" onkeyup = "rechercherCandidature()"type="text" class="form-control" placeholder="Rechercher une candidature">
                </div>
            </div>
    
            <br>
            
            <c:set var="nbrAcceptation" value="0"> </c:set>
            <c:forEach items="${etudiant.liste_candidature}" var="items" >
                
                <c:if test="${!items.getStatut().equals('En attente')}">
                    
                    <c:set var="nbrAcceptation" value="${nbrAcceptation + 1}"></c:set>
                </c:if>
            </c:forEach>
            
            <c:choose>
                <c:when test = "${nbrAcceptation == 0}" >
                Vous n'avez aucune candidature accepté, restez patient. ${nbrAcceptation}
                </c:when> 
                <c:when test = "${nbrAcceptation == 1}" >
                Vous avez une candidature accepté. ${nbrAcceptation}
                </c:when> 
                <c:when test = "${nbrAcceptation > 1}" >
                    Vous avez ${nbrAcceptation} candidatures acceptés, faites votre choix !
                </c:when> 
            </c:choose>
            
                    <br>

    <article class="row">
        <div class="col-12">
            <table id="stage" class="table table-hover">
                <thead>
                    <tr>
                        <th onclick="sortTable(0, 'stage')">Compagnie </th>
                        <th onclick="sortTable(1, 'stage')">Poste </th>
                        <th>Date de contact </th>
                        <th>Statut </th>
                        
                        <th>Confirmation </th>
                        
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${etudiant.liste_candidature}" var="candidature">
                        <fmt:formatDate var="date" value="${candidature.date}" pattern="yyyy-MM-dd" />
                        <c:set var="offre" value="${odao.read(candidature.idOffre)}"/>
                        <c:set var="employeur" value="${edao.read(offre.idEmployeur)}"/>
                        <c:set var="compagnie" value="${cdao.read(employeur.compagnie.idCompagnie)}"/>

                        <tr>
                            <td>${compagnie.nom}</td>
                            <td>${offre.titre}</td>
                            <td>${date}</td>
                            <td>${candidature.statut} </td>
                            <td> 
                            <c:choose>
                            <c:when test = "${candidature.statut.equals('En attente')}" >
                                Aucune confirmation
                            </c:when> 
                            <c:when test = "${candidature.statut.equals('Accepte')}" >
                                <form>
                                    <input type="hidden" id="idEtudiant" name="idEtudiant" value="${sessionScope.connecte}"> 
                                    <input type="hidden" id="idOffre" name ="idOffre" value="${candidature.idOffre}">
                                    <input type="hidden" id="tache" name="tache" value="effectuerConfirmation">
                                    <input type="hidden" id="msgConfirmation" value="Voulez-vous vraiment confirmer pour faire votre stage dans cette entreprise ? Attention, cette action est irréversible.">
                                    <button type="button" onclick="remplirModalConfirmationConfirmerCandidature();" data-toggle="modal" data-target="#modalConfirmation" class="btn my-btn-outline-primary my-2 my-sm-0">Confirmer</button>
                                </form>
                            </c:when> 
                            <c:when test = "${candidature.statut.equals('Confirmer')}" >
                                Félications, vous êtes accepté(e) pour de bon
                            </c:when>
                            </c:choose>
                            </td>       
                            
                            
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </article>
</section>
            
<jsp:include page="/WEB-INF/vue/modals/modalConfirmation.jsp"></jsp:include>

<script>
function rechercherCandidature() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("inputRecherche");
    filter = input.value.toUpperCase();
    table = document.getElementById("stage");
    tr = table.getElementsByTagName("tr");

    for (i = 1; i < tr.length; i++) {
        var td0 = tr[i].getElementsByTagName("td")[0].textContent.toUpperCase();
        var td1 = tr[i].getElementsByTagName("td")[1].textContent.toUpperCase();
        var td2 = tr[i].getElementsByTagName("td")[2].textContent.toUpperCase();
        var td3 = tr[i].getElementsByTagName("td")[3].textContent.toUpperCase();
        if (td0.indexOf(filter) > -1 || td1.indexOf(filter) > -1 || td2.indexOf(filter) > -1 || td3.indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }      
    }
    }  

</script>

<style>
    th{
        cursor:pointer;
    }
</style>
