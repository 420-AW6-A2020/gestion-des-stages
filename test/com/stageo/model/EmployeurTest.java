/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Nicole
 */
public class EmployeurTest {
    Employeur instance;
    
    public EmployeurTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws Exception {
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of Constructeur6Parametres method, of class Employeur.
     */
    @Test
    public void testConstructeur6Parametres() {
        System.out.println("constructeur6Parametres");
        String uniqueID = UUID.randomUUID().toString();
        Compagnie co = new Compagnie();
        instance = new Employeur("aaaa@gmail.com", "Anonyme",
                "Anonyme", "Moi-même", "Moi-même", "Président");        
        assertEquals(instance.getEmail(), "aaaa@gmail.com");
        assertEquals(instance.getNomUtilisateur() , "Anonyme");
        assertEquals(instance.getMotDePasse(), "Anonyme");
        assertEquals(instance.getNom() , "Moi-même");
        assertEquals(instance.getPrenom() , "Moi-même");
        assertEquals(instance.getRole() , "Président");
    }
    
    /**
     * Test of Constructeur7Parametres method, of class Employeur.
     */
    @Test
    public void testConstructeur7Parametres() {
        System.out.println("constructeur7Parametres");        
        instance = new Employeur("123", "aaaa@gmail.com", "Anonyme",
                "Anonyme", "Moi-même", "Moi-même", "Président");        
        assertEquals(instance.getIdUtilisateur(), "123");
        assertEquals(instance.getEmail(), "aaaa@gmail.com");
        assertEquals(instance.getNomUtilisateur() , "Anonyme");
        assertEquals(instance.getMotDePasse(), "Anonyme");
        assertEquals(instance.getNom() , "Moi-même");
        assertEquals(instance.getPrenom() , "Moi-même");
        assertEquals(instance.getRole() , "Président");
    }

    /**
     * Test of setCompagnie method, of class Employeur.
     */
    @Test
    public void testSetCompagnie() {
        System.out.println("setCompagnie");
        Compagnie compagnie = new Compagnie();
        Employeur instance = new Employeur();
        instance.setCompagnie(compagnie);
        
        assertEquals(instance.getCompagnie() , compagnie);
    }

    /**
     * Test of setFonction method, of class Employeur.
     */
    @Test
    public void testSetFonction() {
        System.out.println("setFonction");
        String fonction = " Dev";
        Employeur instance = new Employeur();
        instance.setFonction(fonction);
        
        assertEquals(instance.getFonction() ,fonction);
    }

    /**
     * Test of setProfilCree method, of class Employeur.
     */
    @Test
    public void testSetProfilCree() {
        System.out.println("setProfilCree");
        String profilCree = "aaaaaaaaa";
        Employeur instance = new Employeur();
        instance.setProfilCree(profilCree);
        
        assertEquals(instance.getProfilCree() , profilCree);
    }

    /**
     * Test of setProfilModifie method, of class Employeur.
     */
    @Test
    public void testSetProfilModifie() {
        System.out.println("setProfilModifie");
        String profilModifie = "bbbbbbbbb";
        Employeur instance = new Employeur();
        instance.setProfilModifie(profilModifie);
        
        assertEquals(instance.getProfilModifie() , profilModifie);
    }
    
}
