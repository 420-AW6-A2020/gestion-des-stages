/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Guillaume
 */
public class ProfesseurTest {
    
    private Professeur instance;
    
    public ProfesseurTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Professeur();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setAnglaisEcrit method, of class Professeur.
     */
    @Test
    public void testSetAnglaisEcrit() {
        System.out.println("setAnglaisEcrit");
        String anglaisEcrit = "Parfait";
        
        instance.setAnglaisEcrit(anglaisEcrit);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getAnglaisEcrit(), "Parfait");
    }

    /**
     * Test of setAnglaisParle method, of class Professeur.
     */
    @Test
    public void testSetAnglaisParle() {
        System.out.println("setAnglaisParle");
        String anglaisParle = "";
        
        instance.setAnglaisParle(anglaisParle);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getAnglaisParle(), "");
    }

    /**
     * Test of setMoyenTransport method, of class Professeur.
     */
    @Test
    public void testSetMoyenTransport() {
        System.out.println("setMoyenTransport");
        String moyenTransport = "12345";
        instance.setMoyenTransport(moyenTransport);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getMoyenTransport(), "12345");
    }

    /**
     * Test of setInfosPartagees method, of class Professeur.
     */
    @Test
    public void testSetInfosPartagees() {
        System.out.println("setInfosPartagees");
        String infosPartagees = "Une information à partager";
        instance.setInfosPartagees(infosPartagees);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getInfosPartagees(), "Une information à partager");
    }

    /**
     * Test of setPreferenceSupervision method, of class Professeur.
     */
    @Test
    public void testSetPreferenceSupervision() {
        System.out.println("setPreferenceSupervision");
        String preferenceSupervision = "réseau, prog, réseau et prog";
        instance.setPreferenceSupervision(preferenceSupervision);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getPreferenceSupervision(), "réseau, prog, réseau et prog");
    }

    /**
     * Test of setDispoStageEte method, of class Professeur.
     */
    @Test
    public void testSetDispoStageEte() {
        System.out.println("setDispoStageEte");
        String dispoStageEte = "oui/non";
        instance.setDispoStageEte(dispoStageEte);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getDispoStageEte(), "oui/non");
    }

    /**
     * Test of setVille method, of class Professeur.
     */
    @Test
    public void testSetVille() {
        System.out.println("setVille");
        String ville = "lqhfqkjdsfhkqsjdhfkqjshfqdshfkjqhkjd";
        instance.setVille(ville);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getVille(), "lqhfqkjdsfhkqsjdhfkqjshfqdshfkjqhkjd");
    }

    /**
     * Test of setProfilCree method, of class Professeur.
     */
    @Test
    public void testSetProfilCree() {
        System.out.println("setProfilCree");
        String profilCree = "29/11/2019";
        instance.setProfilCree(profilCree);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getProfilCree(), "29/11/2019");
    }

    /**
     * Test of setProfilModifie method, of class Professeur.
     */
    @Test
    public void testSetProfilModifie() {
        System.out.println("setProfilModifie");
        String profilModifie = "30/11/2019";
        instance.setProfilModifie(profilModifie);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getProfilModifie(), "30/11/2019");
    }

    /**
     * Test of setListeEtudiantSupervise method, of class Professeur.
     */
    @Test
    public void testSetListeEtudiantSupervise() {
        System.out.println("setListeEtudiantSupervise");
        List<Etudiant> listeEtudiantSupervise = null;
        instance.setListeEtudiantSupervise(listeEtudiantSupervise);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getListeEtudiantSupervise(), null);
    }
    
}
